/*
   Copyright 2019 Anthony Stramer <anthony@stramer.org>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */

package org.stramer.lib.poe;

import org.stramer.lib.poe.conf.Configuration;

class AccountImpl extends GenericDeserializer<Account> implements Account {
    private String name;
    private String realm;
    private Challenges challenges;
    private Twitch twitch;

    AccountImpl(Configuration configuration) {
        super(configuration);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getRealm() {
        return realm;
    }

    @Override
    public Challenges getChallenges() {
        return challenges;
    }

    @Override
    public Twitch getTwitch() {
        return twitch;
    }

    @Override
    protected Account deserialize() {
        AccountImpl account = new AccountImpl(configuration);
        account.name = deserializeRequired("name", String.class);
        account.realm = deserializeRequired("realm", String.class);
        account.challenges = deserializeRequired("challenges", Challenges.class);
        account.twitch = deserializeOptional("twitch", Twitch.class);
        return account;
    }

    @Override
    public String toString() {
        return "AccountImpl{" +
                "name='" + name + '\'' +
                ", realm='" + realm + '\'' +
                ", challenges=" + challenges +
                ", twitch=" + twitch +
                '}';
    }
}
