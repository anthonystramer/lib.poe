/*
   Copyright 2019 Anthony Stramer <anthony@stramer.org>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */

package org.stramer.lib.poe;

import org.stramer.lib.poe.conf.Configuration;

class ValueImpl extends GenericDeserializer<Value> implements Value {
    private String value;
    private int valueType;

    ValueImpl(Configuration configuration) {
        super(configuration);
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public int getValueType() {
        return valueType;
    }

    @Override
    protected Value deserialize() {
        ValueImpl value = new ValueImpl(configuration);
        value.value = getJson().getAsJsonArray().get(0).getAsString();
        value.valueType = getJson().getAsJsonArray().get(1).getAsInt();
        return value;
    }

    @Override
    public String toString() {
        return "ValueImpl{" +
                "value='" + value + '\'' +
                ", valueType=" + valueType +
                '}';
    }
}
