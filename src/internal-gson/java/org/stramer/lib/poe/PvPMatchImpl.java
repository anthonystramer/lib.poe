/*
   Copyright 2019 Anthony Stramer <anthony@stramer.org>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */

package org.stramer.lib.poe;

import org.stramer.lib.poe.conf.Configuration;

import java.net.URL;
import java.util.Date;

class PvPMatchImpl extends GenericDeserializer<PvPMatch> implements PvPMatch {
    private String id;
    private String realm;
    private Date startAt;
    private Date endAt;
    private URL url;
    private String description;
    private boolean glickoRatings;
    private boolean pvp;
    private String style;
    private Date registerAt;

    PvPMatchImpl(Configuration configuration) {
        super(configuration);
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getRealm() {
        return realm;
    }

    @Override
    public Date getStartAt() {
        return startAt;
    }

    @Override
    public Date getEndAt() {
        return endAt;
    }

    @Override
    public URL getUrl() {
        return url;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public boolean isGlickoRatings() {
        return glickoRatings;
    }

    @Override
    public boolean isPvp() {
        return pvp;
    }

    @Override
    public String getStyle() {
        return style;
    }

    @Override
    public Date getRegisterAt() {
        return registerAt;
    }

    @Override
    protected PvPMatch deserialize() {
        PvPMatchImpl pvpMatch = new PvPMatchImpl(configuration);
        pvpMatch.id = deserializeRequired("id", String.class);
        pvpMatch.realm = deserializeRequired("realm", String.class);
        pvpMatch.startAt = deserializeRequired("startAt", Date.class);
        pvpMatch.endAt = deserializeRequired("endAt", Date.class);
        pvpMatch.url = deserializeRequired("url", URL.class);
        pvpMatch.description = deserializeRequired("description", String.class);
        pvpMatch.glickoRatings = deserializeRequired("glickoRatings", Boolean.class);
        pvpMatch.pvp = deserializeRequired("pvp", Boolean.class);
        pvpMatch.style = deserializeRequired("style", String.class);
        pvpMatch.registerAt = deserializeRequired("registerAt", Date.class);
        return pvpMatch;
    }

    @Override
    public String toString() {
        return "PvPMatchImpl{" +
                "id='" + id + '\'' +
                ", realm='" + realm + '\'' +
                ", startAt=" + startAt +
                ", endAt=" + endAt +
                ", url=" + url +
                ", description='" + description + '\'' +
                ", glickoRatings=" + glickoRatings +
                ", pvp=" + pvp +
                ", style='" + style + '\'' +
                ", registerAt=" + registerAt +
                '}';
    }
}
