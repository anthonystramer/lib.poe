/*
   Copyright 2019 Anthony Stramer <anthony@stramer.org>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */

package org.stramer.lib.poe;

import org.stramer.lib.poe.conf.Configuration;

class CharacterImpl extends GenericDeserializer<Character> implements Character {
    private String name;
    private int level;
    private String characterClass;
    private String id;
    private long experience;
    private Depth depth;

    CharacterImpl(Configuration configuration) {
        super(configuration);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getLevel() {
        return level;
    }

    @Override
    public String getCharacterClass() {
        return characterClass;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public long getExperience() {
        return experience;
    }

    @Override
    public Depth getDepth() {
        return depth;
    }

    @Override
    protected Character deserialize() {
        CharacterImpl character = new CharacterImpl(configuration);
        character.name = deserializeRequired("name", String.class);
        character.level = deserializeRequired("level", Integer.class);
        character.characterClass = deserializeRequired("class", String.class);
        character.id = deserializeRequired("id", String.class);
        character.experience = deserializeRequired("experience", Long.class);
        character.depth = deserializeOptional("depth", Depth.class);
        return character;
    }

    @Override
    public String toString() {
        return "CharacterImpl{" +
                "name='" + name + '\'' +
                ", level=" + level +
                ", characterClass='" + characterClass + '\'' +
                ", id='" + id + '\'' +
                ", experience=" + experience +
                ", depth=" + depth +
                '}';
    }
}
