/*
   Copyright 2019 Anthony Stramer <anthony@stramer.org>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */

package org.stramer.lib.poe;

import org.stramer.lib.poe.conf.Configuration;

import java.net.URL;
import java.util.Date;

class LeagueImpl extends GenericDeserializer<League> implements League {
    private String id;
    private String realm;
    private String description;
    private URL url;
    private Date startAt;
    private Date endAt;
    private boolean delveEvent;
    private LeagueRulesList rules;

    LeagueImpl(Configuration configuration) {
        super(configuration);
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getRealm() {
        return realm;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public URL getUrl() {
        return url;
    }

    @Override
    public Date getStartAt() {
        return startAt;
    }

    @Override
    public Date getEndAt() {
        return endAt;
    }

    @Override
    public boolean getDelveEvent() {
        return delveEvent;
    }

    @Override
    public LeagueRulesList getRules() {
        return rules;
    }

    @Override
    protected League deserialize() {
        LeagueImpl league = new LeagueImpl(configuration);
        league.id = deserializeRequired("id", String.class);
        league.realm = deserializeRequired("realm", String.class);
        league.description = deserializeRequired("description", String.class);
        league.url = deserializeRequired("url", URL.class);
        league.startAt = deserializeRequired("startAt", Date.class);
        league.endAt = deserializeRequired("endAt", Date.class);
        league.delveEvent = deserializeRequired("delveEvent", Boolean.class);
        league.rules = deserializeRequired("rules", LeagueRulesList.class);
        return league;
    }

    @Override
    public String toString() {
        return "LeagueImpl{" +
                "id='" + id + '\'' +
                ", realm='" + realm + '\'' +
                ", description='" + description + '\'' +
                ", url=" + url +
                ", startAt=" + startAt +
                ", endAt=" + endAt +
                ", delveEvent=" + delveEvent +
                ", rules=" + rules +
                '}';
    }
}
