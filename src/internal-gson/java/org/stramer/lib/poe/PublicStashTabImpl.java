/*
   Copyright 2019 Anthony Stramer <anthony@stramer.org>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */

package org.stramer.lib.poe;

import org.stramer.lib.poe.conf.Configuration;

class PublicStashTabImpl extends GenericDeserializer<PublicStashTab> implements PublicStashTab {
    private String id;
    private boolean isPublic;
    private String accountName;
    private String lastCharacterName;
    private String stash;
    private String stashType;
    private String league;
    private ItemsList itemsList;

    PublicStashTabImpl(Configuration configuration) {
        super(configuration);
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public boolean isPublic() {
        return isPublic;
    }

    @Override
    public String getAccountName() {
        return accountName;
    }

    @Override
    public String getLastCharacterName() {
        return lastCharacterName;
    }

    @Override
    public String getStash() {
        return stash;
    }

    @Override
    public String getStashType() {
        return stashType;
    }

    @Override
    public String getLeague() {
        return league;
    }

    @Override
    public ItemsList getItemsList() {
        return itemsList;
    }

    @Override
    protected PublicStashTab deserialize() {
        PublicStashTabImpl publicStashTab = new PublicStashTabImpl(configuration);
        publicStashTab.id = deserializeRequired("id", String.class);
        publicStashTab.isPublic = deserializeRequired("public", Boolean.class);
        publicStashTab.accountName = deserializeRequired("accountName", String.class);
        publicStashTab.lastCharacterName =  deserializeRequired("lastCharacterName", String.class);
        publicStashTab.stash = deserializeRequired("stash", String.class);
        publicStashTab.stashType = deserializeRequired("stashType", String.class);
        publicStashTab.league = deserializeRequired("league", String.class);
        publicStashTab.itemsList = deserializeRequired("items", ItemsList.class);
        return publicStashTab;
    }

    @Override
    public String toString() {
        return "PublicStashTabImpl{" +
                "id='" + id + '\'' +
                ", isPublic=" + isPublic +
                ", accountName='" + accountName + '\'' +
                ", lastCharacterName='" + lastCharacterName + '\'' +
                ", stash='" + stash + '\'' +
                ", stashType='" + stashType + '\'' +
                ", league='" + league + '\'' +
                ", itemsList=" + itemsList +
                '}';
    }
}
