/*
   Copyright 2019 Anthony Stramer <anthony@stramer.org>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */

package org.stramer.lib.poe;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.stramer.lib.poe.api.*;
import org.stramer.lib.poe.conf.Configuration;
import org.stramer.lib.poe.conf.ConfigurationContext;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

class PathOfExileImpl implements PathOfExile {
    private final Configuration conf;
    private final Gson gson;
    private Logger logger;

    PathOfExileImpl(Configuration conf) {
        this.conf = conf;
        this.gson = new GsonBuilder()
                            .registerTypeAdapter(Account.class, new AccountImpl(conf))
                            .registerTypeAdapter(Category.class, new CategoryImpl(conf))
                            .registerTypeAdapter(Challenges.class, new ChallengesImpl(conf))
                            .registerTypeAdapter(Character.class, new CharacterImpl(conf))
                            .registerTypeAdapter(Depth.class, new DepthImpl(conf))
                            .registerTypeAdapter(ModsList.class, new ModsListImpl())
                            .registerTypeAdapter(FlavourTextList.class, new FlavourTextListImpl())
                            .registerTypeAdapter(Item.class, new ItemImpl(conf))
                            .registerTypeAdapter(ItemsList.class, new ItemsListImpl())
                            .registerTypeAdapter(Ladder.class, new LadderImpl(conf))
                            .registerTypeAdapter(LaddersList.class, new LaddersListImpl())
                            .registerTypeAdapter(League.class, new LeagueImpl(conf))
                            .registerTypeAdapter(LeagueRulesList.class, new LeagueRulesListImpl())
                            .registerTypeAdapter(LeagueRule.class, new LeagueRuleImpl(conf))
                            .registerTypeAdapter(LeaguesList.class, new LeaguesListImpl())
                            .registerTypeAdapter(PropertiesList.class, new PropertiesListImpl())
                            .registerTypeAdapter(Property.class, new PropertyImpl(conf))
                            .registerTypeAdapter(PublicStashTab.class, new PublicStashTabImpl(conf))
                            .registerTypeAdapter(PublicStashTabList.class, new PublicStashTabListImpl())
                            .registerTypeAdapter(PvPMatch.class, new PvPMatchImpl(conf))
                            .registerTypeAdapter(PvPMatchesList.class, new PvPMatchesListImpl())
                            .registerTypeAdapter(Socket.class, new SocketImpl(conf))
                            .registerTypeAdapter(SocketedItemsList.class, new SocketedItemsListImpl())
                            .registerTypeAdapter(SocketsList.class, new SocketsListImpl())
                            .registerTypeAdapter(Twitch.class, new TwitchImpl(conf))
                            .registerTypeAdapter(Value.class, new ValueImpl(conf))
                            .registerTypeAdapter(ValuesList.class, new ValuesListImpl())
                            .create();

        /* TODO seems messy to do this in addition to pushing the conf to all of the adapters, plus peepz can
                can overwrite the configuration in the middle of running
         */
        ConfigurationContext.setConfiguration(conf);
        logger = Logger.getLogger(PathOfExileImpl.class);
    }


    /* Resources */


    public LaddersResources ladders() {
        return this;
    }

    public LeagueRulesResources leagueRules() {
        return this;
    }

    public LeaguesResources leagues() {
        return this;
    }

    public PublicStashTabsResources publicStashTabs() {
        return this;
    }

    public PvPMatchesResources pvpMatches() {
        return this;
    }

    @Override
    public Configuration getConfiguration() {
        return conf;
    }


    /* Ladders Resources */


    public LaddersList getLadders(String id) throws PathOfExileException {
        return getLadders(id, new LaddersResourcesOptions());
    }

    public LaddersList getLadders(String id, LaddersResourcesOptions options) throws PathOfExileException {
        return getAPIResponse("/ladders/" + id, options.toMap(), LaddersList.class);
    }


    /* League Rules Resources */


    public LeagueRulesList getLeagueRules() throws PathOfExileException {
        return getAPIResponse("/league-rules", LeagueRulesList.class);
    }

    public LeagueRule getLeagueRule(String id) throws PathOfExileException {
        return getAPIResponse("/league-rules/" + id, LeagueRule.class);
    }


    /* League Resources */


    public LeaguesList getLeagues() throws PathOfExileException {
        return getLeagues(new ListLeaguesResourcesOptions());
    }

    public LeaguesList getLeagues(ListLeaguesResourcesOptions options) throws PathOfExileException {
        return getAPIResponse("/leagues", options.toMap(), LeaguesList.class);
    }

    public League getLeague(String id) throws PathOfExileException {
        return getLeague(id, new GetLeaguesResourcesOptions());
    }

    public League getLeague(String id, GetLeaguesResourcesOptions options) throws PathOfExileException {
        return getAPIResponse("/leagues/" + id, options.toMap(), League.class);
    }


    /* Public Stash Tab Resources */


    public PublicStashTabList getPublicStashTabs() throws PathOfExileException {
        return getPublicStashTabs(new PublicStashTabsResourcesOptions());
    }

    public PublicStashTabList getPublicStashTabs(PublicStashTabsResourcesOptions options) throws PathOfExileException {
        return getAPIResponse("/public-stash-tabs", options.toMap(), PublicStashTabList.class);
    }


    /* PvP Matches Resources */


    public PvPMatchesList getPvPMatches() throws PathOfExileException {
        return getPvPMatches(new PvPMatchesResourcesOptions());
    }

    public PvPMatchesList getPvPMatches(PvPMatchesResourcesOptions options) throws PathOfExileException {
        return getAPIResponse("/pvp-matches", options.toMap(), PvPMatchesList.class);
    }



    /* Helpers */

    private <T> T getAPIResponse(String path, Class<T> classOfT) throws PathOfExileException {
        return getAPIResponse(path, new HashMap<>(), classOfT);
    }

    private <T> T getAPIResponse(String path, Map<String, String> queryParameters, Class<T> classOfT) throws PathOfExileException {
        URL url;
        InputStreamReader reader;

        try {
            StringBuilder query = new StringBuilder();
            for(Map.Entry<String, String> entry : queryParameters.entrySet()) {
                if(entry.getValue() != null) {
                    if(query.length() > 0) query.append("&");
                    query.append(entry.getKey());
                    query.append("=");
                    query.append(entry.getValue());
                }
            }

            url = new URL(conf.getBaseURL() + path + (query.length() > 0 ? "?" + query.toString() : ""));
        } catch (MalformedURLException e) {
            throw new PathOfExileException(e);
        }

        try {
            logger.debug("HTTP GET " + url);
            reader = new InputStreamReader(url.openStream());
        } catch (IOException e) {
            throw new PathOfExileException(e);
        }

        return processAPIResponse(reader, classOfT);
    }

    protected  <T> T processAPIResponse(InputStreamReader reader, Class<T> classOfT) throws PathOfExileException {
        T response = gson.fromJson(reader, classOfT);

        try {
            reader.close();
        } catch (IOException e) {
            throw new PathOfExileException(e);
        }

        return response;
    }
}
