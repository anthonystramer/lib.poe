/*
   Copyright 2019 Anthony Stramer <anthony@stramer.org>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */

package org.stramer.lib.poe;

import org.stramer.lib.poe.conf.Configuration;

class LadderImpl extends GenericDeserializer<Ladder> implements Ladder {
    private int rank;
    private boolean dead;
    private boolean online;
    private Character character;
    private Account account;

    LadderImpl(Configuration configuration) {
        super(configuration);
    }

    public int getRank() {
        return rank;
    }

    public boolean isDead() {
        return dead;
    }

    public boolean isOnline() {
        return online;
    }

    public Character getCharacter() {
        return character;
    }

    public Account getAccount() {
        return account;
    }

    @Override
    protected Ladder deserialize() {
        LadderImpl ladder = new LadderImpl(configuration);
        ladder.rank = deserializeRequired("rank", Integer.class);
        ladder.dead = deserializeRequired("dead", Boolean.class);
        ladder.online = deserializeRequired("online", Boolean.class);
        ladder.character = deserializeRequired("character", Character.class);
        ladder.account = deserializeRequired("account", Account.class);
        return ladder;
    }

    @Override
    public String toString() {
        return "LadderImpl{" +
                "rank=" + rank +
                ", dead=" + dead +
                ", online=" + online +
                ", character=" + character +
                ", account=" + account +
                '}';
    }
}
