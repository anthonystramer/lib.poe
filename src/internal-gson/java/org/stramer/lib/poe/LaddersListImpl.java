/*
   Copyright 2019 Anthony Stramer <anthony@stramer.org>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */

package org.stramer.lib.poe;

import com.google.gson.*;

import java.lang.reflect.Type;
import java.util.Date;

class LaddersListImpl extends ResponseListImpl<Ladder> implements LaddersList, JsonDeserializer<LaddersList> {
    private int total;
    private Date cachedSince;

    public int getTotal() {
        return total;
    }

    public Date getCachedSince() {
        return cachedSince;
    }

    @Override
    public LaddersList deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        LaddersListImpl laddersList = new LaddersListImpl();
        laddersList.total = json.getAsJsonObject().get("total").getAsInt();
        laddersList.cachedSince = context.deserialize(json.getAsJsonObject().get("cached_since"), Date.class);
        for(JsonElement jsonElement : json.getAsJsonObject().get("entries").getAsJsonArray()) {
            laddersList.add(context.deserialize(jsonElement, Ladder.class));
        }
        return laddersList;
    }

    @Override
    public String toString() {
        return "LaddersListImpl{" +
                "total=" + total +
                ", cachedSince=" + cachedSince +
                ", entries=" + super.toString() +
                '}';
    }
}
