/*
   Copyright 2019 Anthony Stramer <anthony@stramer.org>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */

package org.stramer.lib.poe;

import org.stramer.lib.poe.conf.Configuration;

import java.util.ArrayList;
import java.util.List;

class CategoryImpl extends GenericDeserializer<Category> implements Category {
    private List<String> accessories;
    private List<String> armour;
    private List<String> jewels;
    private List<String> weapons;
    private List<String> cards;
    private List<String> currency;
    private List<String> flasks;
    private List<String> gems;
    private List<String> maps;
    private List<String> monsters;

    CategoryImpl(Configuration configuration) {
        super(configuration);
    }

    @Override
    public List<String> getAccessories() {
        return accessories;
    }

    @Override
    public List<String> getArmour() {
        return armour;
    }

    @Override
    public List<String> getJewels() {
        return jewels;
    }

    @Override
    public List<String> getWeapons() {
        return weapons;
    }

    @Override
    public List<String> getCards() {
        return cards;
    }

    @Override
    public List<String> getCurrency() {
        return currency;
    }

    @Override
    public List<String> getFlasks() {
        return flasks;
    }

    @Override
    public List<String> getGems() {
        return gems;
    }

    @Override
    public List<String> getMaps() {
        return maps;
    }

    @Override
    public List<String> getMonsters() {
        return monsters;
    }

    @Override
    protected Category deserialize() {
        CategoryImpl category = new CategoryImpl(configuration);
        category.accessories = deserializeOptional("accessories", ArrayList.class);
        category.armour = deserializeOptional("armour", ArrayList.class);
        category.jewels = deserializeOptional("jewels", ArrayList.class);
        category.weapons = deserializeOptional("weapons", ArrayList.class);
        category.cards = deserializeOptional("cards", ArrayList.class);
        category.currency = deserializeOptional("currency", ArrayList.class);
        category.flasks = deserializeOptional("flasks", ArrayList.class);
        category.gems = deserializeOptional("gems", ArrayList.class);
        category.maps = deserializeOptional("maps", ArrayList.class);
        category.monsters = deserializeOptional("monsters", ArrayList.class);
        return category;
    }

    @Override
    public String toString() {
        return "CategoryImpl{" +
                "accessories=" + accessories +
                ", armour=" + armour +
                ", jewels=" + jewels +
                ", weapons=" + weapons +
                ", cards=" + cards +
                ", currency=" + currency +
                ", flasks=" + flasks +
                ", gems=" + gems +
                ", maps=" + maps +
                ", monsters=" + monsters +
                '}';
    }
}
