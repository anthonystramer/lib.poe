/*
   Copyright 2019 Anthony Stramer <anthony@stramer.org>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */

package org.stramer.lib.poe;

import org.stramer.lib.poe.conf.Configuration;

class LeagueRuleImpl extends GenericDeserializer<LeagueRule> implements LeagueRule {
    private String id;
    private String name;
    private String description;

    LeagueRuleImpl(Configuration configuration) {
        super(configuration);
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    @Override
    protected LeagueRule deserialize() {
        LeagueRuleImpl leagueRule = new LeagueRuleImpl(configuration);
        leagueRule.id = deserializeRequired("id", String.class);
        leagueRule.name = deserializeRequired("name", String.class);
        leagueRule.description = deserializeRequired("description", String.class);
        return leagueRule;
    }

    @Override
    public String toString() {
        return "LeagueRuleImpl{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
