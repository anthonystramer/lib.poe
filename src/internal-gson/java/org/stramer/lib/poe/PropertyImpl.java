/*
   Copyright 2019 Anthony Stramer <anthony@stramer.org>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */

package org.stramer.lib.poe;

import org.stramer.lib.poe.conf.Configuration;

class PropertyImpl extends GenericDeserializer<Property> implements Property {
    private String name;
    private ValuesList values;
    private int displayMode;
    private Integer type;
    private Double progress;

    PropertyImpl(Configuration configuration) {
        super(configuration);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public ValuesList getValues() {
        return values;
    }

    @Override
    public int getDisplayMode() {
        return displayMode;
    }

    @Override
    public Integer getType() {
        return type;
    }

    @Override
    public Double getProgress() {
        return progress;
    }


    @Override
    protected Property deserialize() {
        PropertyImpl property = new PropertyImpl(configuration);
        property.name = deserializeRequired("name", String.class);
        property.values = deserializeRequired("values", ValuesList.class);
        property.displayMode = deserializeRequired("displayMode", Integer.class);
        property.type = deserializeOptional("type", Integer.class);
        property.progress = deserializeOptional("progress", Double.class);
        return property;
    }

    @Override
    public String toString() {
        return "PropertyImpl{" +
                "name='" + name + '\'' +
                ", values=" + values +
                ", displayMode=" + displayMode +
                ", type=" + type +
                ", progress=" + progress +
                '}';
    }
}
