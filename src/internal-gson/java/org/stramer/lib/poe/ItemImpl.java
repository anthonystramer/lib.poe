/*
   Copyright 2019 Anthony Stramer <anthony@stramer.org>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */

package org.stramer.lib.poe;

import org.stramer.lib.poe.conf.Configuration;

import java.net.URL;

class ItemImpl extends GenericDeserializer<Item> implements Item {
    private boolean verified;
    private int w;
    private int h;
    private int ilvl;
    private URL icon;
    private boolean support;
    private String league;
    private String id;
    private Boolean elder;
    private Boolean fractured;
    private Boolean shaper;
    private Boolean abyssJewel;
    private Boolean delve;
    private SocketsList sockets;
    private String name;
    private String typeLine;
    private Boolean duplicated;
    private boolean identified;
    private String note;
    private Boolean corrupted;
    private PropertiesList properties;
    private PropertiesList additionalProperties;
    private PropertiesList requirements;
    private PropertiesList nextLevelRequirements;
    private String secDescrText;
    private ModsList utilityMods;
    private ModsList implicitMods;
    private ModsList explicitMods;
    private ModsList craftedMods;
    private ModsList fracturedMods;
    private String descrText;
    private FlavourTextList flavourText;
    private String prophecyText;
    private int frameType;
    private Integer stackSize;
    private Integer maxStackSize;
    private String artFilename;
    private Category category;
    private Integer x;
    private Integer y;
    private String inventoryId;
    private SocketedItemsList socketedItems;
    private String colour;

    ItemImpl(Configuration configuration) {
        super(configuration);
    }

    @Override
    public boolean isVerified() {
        return verified;
    }

    @Override
    public int getW() {
        return w;
    }

    @Override
    public int getH() {
        return h;
    }

    @Override
    public int getIlvl() {
        return ilvl;
    }

    @Override
    public URL getIcon() {
        return icon;
    }

    @Override
    public boolean isSupport() {
        return support;
    }

    @Override
    public String getLeague() {
        return league;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public Boolean isElder() {
        return elder;
    }

    @Override
    public Boolean isShaper() {
        return shaper;
    }

    @Override
    public Boolean isFractured() {
        return fractured;
    }

    @Override
    public Boolean isAbyssJewel() {
        return abyssJewel;
    }

    @Override
    public Boolean isDelve() {
        return delve;
    }

    @Override
    public SocketsList getSockets() {
        return sockets;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getTypeLine() {
        return typeLine;
    }

    @Override
    public Boolean isDuplicated() {
        return duplicated;
    }

    @Override
    public boolean isIdentified() {
        return identified;
    }

    @Override
    public String getNote() {
        return note;
    }

    @Override
    public Boolean isCorrupted() {
        return corrupted;
    }

    @Override
    public PropertiesList getProperties() {
        return properties;
    }

    @Override
    public PropertiesList getAdditionalProperties() {
        return additionalProperties;
    }

    @Override
    public PropertiesList getRequirements() {
        return requirements;
    }

    @Override
    public PropertiesList getNextLevelRequirements() {
        return nextLevelRequirements;
    }

    @Override
    public String secDescrText() {
        return secDescrText;
    }

    @Override
    public ModsList getUtilityMods() {
        return utilityMods;
    }

    @Override
    public ModsList getImplicitMods() {
        return implicitMods;
    }

    @Override
    public ModsList getExplicitMods() {
        return explicitMods;
    }

    @Override
    public ModsList getCraftedMods() {
        return craftedMods;
    }

    @Override
    public ModsList getFracturedMods() {
        return fracturedMods;
    }

    @Override
    public String getDescrText() {
        return descrText;
    }

    @Override
    public FlavourTextList getFlavourText() {
        return flavourText;
    }

    @Override
    public String getProphecyText() {
        return prophecyText;
    }

    @Override
    public int getFrameType() {
        return frameType;
    }

    @Override
    public Integer getStackSize() {
        return stackSize;
    }

    @Override
    public Integer getMaxStackSize() {
        return maxStackSize;
    }

    @Override
    public String getArtFilename() {
        return artFilename;
    }

    @Override
    public Category getCategory() {
        return category;
    }

    @Override
    public Integer getX() {
        return x;
    }

    @Override
    public Integer getY() {
        return y;
    }

    @Override
    public String getInventoryId() {
        return inventoryId;
    }

    @Override
    public SocketedItemsList getSocketedItems() {
        return socketedItems;
    }

    @Override
    public String getColour() {
        return colour;
    }

    @Override
    protected Item deserialize() {
        ItemImpl item = new ItemImpl(configuration);
        item.verified = deserializeRequired("verified", Boolean.class);
        item.w = deserializeRequired("w", Integer.class);
        item.h = deserializeRequired("h", Integer.class);
        item.ilvl = deserializeRequired("ilvl", Integer.class);
        item.icon = deserializeRequired("icon", URL.class);
        item.support = deserializeOptional("support", Boolean.class, Boolean.FALSE);
        item.league = deserializeRequired("league", String.class);
        item.id = deserializeRequired("id", String.class);
        item.elder = deserializeOptional("elder", Boolean.class);
        item.fractured = deserializeOptional("fractured", Boolean.class);
        item.shaper = deserializeOptional("shaper", Boolean.class);
        item.abyssJewel = deserializeOptional("abyssJewel", Boolean.class);
        item.delve = deserializeOptional("delve", Boolean.class);
        item.sockets = deserializeOptional("sockets", SocketsList.class);
        item.name = deserializeRequired("name", String.class);
        item.typeLine = deserializeRequired("typeLine", String.class);
        item.duplicated = deserializeOptional("duplicated", Boolean.class);
        item.identified = deserializeRequired("identified", Boolean.class);
        item.note = deserializeOptional("note", String.class);
        item.corrupted = deserializeOptional("corrupted", Boolean.class);
        item.properties = deserializeRequired("properties", PropertiesList.class);
        item.additionalProperties = deserializeRequired("additionalProperties", PropertiesList.class);
        item.requirements = deserializeRequired("requirements", PropertiesList.class);
        item.nextLevelRequirements = deserializeRequired("nextLevelRequirements", PropertiesList.class);
        item.secDescrText = deserializeOptional("secDescrText", String.class);
        item.utilityMods = deserializeOptional("utilityMods", ModsList.class);;
        item.implicitMods = deserializeRequired("implicitMods", ModsList.class);
        item.explicitMods = deserializeRequired("explicitMods", ModsList.class);
        item.craftedMods = deserializeOptional("craftedMods", ModsList.class);
        item.fracturedMods = deserializeRequired("fracturedMods", ModsList.class);
        item.descrText = deserializeOptional("descrText", String.class);
        item.flavourText = deserializeRequired("flavourText", FlavourTextList.class);
        item.prophecyText = deserializeOptional("prophecyText", String.class);
        item.frameType = deserializeRequired("frameType", Integer.class);
        item.stackSize = deserializeOptional("stackSize", Integer.class);
        item.maxStackSize = deserializeOptional("maxStackSize", Integer.class);
        item.artFilename = deserializeOptional("artFilename", String.class);
        item.category = deserializeOptional("category", Category.class);
        item.x = deserializeOptional("x", Integer.class);
        item.y = deserializeOptional("y", Integer.class);
        item.inventoryId = deserializeOptional("inventoryId", String.class);
        item.socketedItems = deserializeRequired("socketedItems", SocketedItemsList.class);
        item.colour = deserializeOptional("colour", String.class);
        return item;
    }

    @Override
    public String toString() {
        return "ItemImpl{" +
                "verified=" + verified +
                ", w=" + w +
                ", h=" + h +
                ", ilvl=" + ilvl +
                ", icon=" + icon +
                ", support=" + support +
                ", league='" + league + '\'' +
                ", id='" + id + '\'' +
                ", elder=" + elder +
                ", fractured=" + fractured +
                ", shaper=" + shaper +
                ", abyssJewel=" + abyssJewel +
                ", delve=" + delve +
                ", sockets=" + sockets +
                ", name='" + name + '\'' +
                ", typeLine='" + typeLine + '\'' +
                ", duplicated=" + duplicated +
                ", identified=" + identified +
                ", note='" + note + '\'' +
                ", corrupted=" + corrupted +
                ", properties=" + properties +
                ", additionalProperties=" + additionalProperties +
                ", requirements=" + requirements +
                ", nextLevelRequirements=" + nextLevelRequirements +
                ", secDescrText='" + secDescrText + '\'' +
                ", utilityMods=" + utilityMods +
                ", implicitMods=" + implicitMods +
                ", explicitMods=" + explicitMods +
                ", craftedMods=" + craftedMods +
                ", fracturedMods=" + fracturedMods +
                ", descrText='" + descrText + '\'' +
                ", flavourText=" + flavourText +
                ", prophecyText='" + prophecyText + '\'' +
                ", frameType=" + frameType +
                ", stackSize=" + stackSize +
                ", maxStackSize=" + maxStackSize +
                ", artFilename='" + artFilename + '\'' +
                ", category=" + category +
                ", x=" + x +
                ", y=" + y +
                ", inventoryId='" + inventoryId + '\'' +
                ", socketedItems=" + socketedItems +
                ", colour='" + colour + '\'' +
                '}';
    }
}
