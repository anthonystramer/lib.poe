/*
   Copyright 2019 Anthony Stramer <anthony@stramer.org>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */

package org.stramer.lib.poe;

import org.stramer.lib.poe.conf.Configuration;

class DepthImpl extends GenericDeserializer<Depth> implements Depth {
    private int defaultDepth;
    private int solo;

    DepthImpl(Configuration configuration) {
        super(configuration);
    }

    @Override
    public int getDefault() {
        return defaultDepth;
    }

    @Override
    public int getSolo() {
        return solo;
    }

    @Override
    protected Depth deserialize() {
        DepthImpl depth = new DepthImpl(configuration);
        depth.defaultDepth = deserializeRequired("default", Integer.class);
        depth.solo = deserializeRequired("solo", Integer.class);
        return depth;
    }

    @Override
    public String toString() {
        return "DepthImpl{" +
                "default=" + defaultDepth +
                ", solo=" + solo +
                '}';
    }
}
