/*
   Copyright 2019 Anthony Stramer <anthony@stramer.org>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */

package org.stramer.lib.poe;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import org.stramer.lib.poe.conf.Configuration;
import org.stramer.lib.poe.conf.ProcessingStrictness;

import java.lang.reflect.Type;
import java.util.HashSet;
import java.util.Set;

abstract class GenericDeserializer<T> implements JsonDeserializer<T> {
    protected Configuration configuration;
    private final Logger LOGGER = Logger.getLogger(GenericDeserializer.class);
    private Type typeOfT;
    private JsonDeserializationContext context;
    private JsonElement json;
    private Set<String> seenKeys;

    GenericDeserializer(Configuration configuration) {
        this.configuration = configuration;
    }

    protected Type getTypeOfT() {
        return typeOfT;
    }

    protected JsonDeserializationContext getContext() {
        return context;
    }

    protected JsonElement getJson() {
        return json;
    }

    protected abstract T deserialize();

    @Override
    public final T deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        this.json = json;
        this.typeOfT = typeOfT;
        this.context = context;

        if(configuration.getProcessingStrictness().equals(ProcessingStrictness.STRICT))
        {
            seenKeys = new HashSet<>();
        }

        LOGGER.debug("Deserializing type: " + typeOfT + " value: " + json.toString());
        T value = deserialize();

        if(configuration.getProcessingStrictness().equals(ProcessingStrictness.STRICT) && json.isJsonObject()) {
            for(String key : json.getAsJsonObject().keySet()) {
                if(!seenKeys.contains(key)) {
                    LOGGER.warn("Unprocessed key in request, key: " + key);
                    LOGGER.info(json.toString());
                }
            }
        }

        return value;
    }

    protected final <F> F deserializeRequired(String memberName, Type typeOfF) {
        F member = context.deserialize(json.getAsJsonObject().get(memberName), typeOfF);
        if(configuration.getProcessingStrictness().equals(ProcessingStrictness.STRICT)) seenKeys.add(memberName);
        return member;
    }

    protected final <F> F deserializeOptional(String memberName, Type typeOfF) {
        return deserializeOptional(memberName, typeOfF, null);
    }

    protected final <F> F deserializeOptional(String memberName, Type typeOfF, F defaultValue) {
        if(json.getAsJsonObject().has(memberName)) {
            if(configuration.getProcessingStrictness().equals(ProcessingStrictness.STRICT)) seenKeys.add(memberName);
            return context.deserialize(json.getAsJsonObject().get(memberName), typeOfF);
        }else{
            return defaultValue;
        }
    }
}
