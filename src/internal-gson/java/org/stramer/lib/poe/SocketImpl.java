/*
   Copyright 2019 Anthony Stramer <anthony@stramer.org>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */

package org.stramer.lib.poe;

import org.stramer.lib.poe.conf.Configuration;

class SocketImpl extends GenericDeserializer<Socket> implements Socket {
    private int group;
    private String attr;
    private String sColour;

    SocketImpl(Configuration configuration) {
        super(configuration);
    }

    public int getGroup() {
        return group;
    }

    public String getAttr() {
        return attr;
    }

    public String getsColour() {
        return sColour;
    }

    @Override
    protected Socket deserialize() {
        SocketImpl socket = new SocketImpl(configuration);
        socket.group = deserializeRequired("group", Integer.class);
        socket.attr = deserializeRequired("attr", String.class);
        socket.sColour = deserializeRequired("sColour", String.class);
        return socket;
    }

    @Override
    public String toString() {
        return "SocketImpl{" +
                "group=" + group +
                ", attr='" + attr + '\'' +
                ", sColour='" + sColour + '\'' +
                '}';
    }
}
