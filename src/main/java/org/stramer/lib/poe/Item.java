/*
   Copyright 2019 Anthony Stramer <anthony@stramer.org>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */

package org.stramer.lib.poe;

import java.net.URL;

public interface Item extends PathOfExileResponse {
    boolean isVerified();
    int getW();
    int getH();
    int getIlvl();
    URL getIcon();
    boolean isSupport();
    String getLeague();
    String getId();
    Boolean isElder();
    Boolean isShaper();
    Boolean isFractured();
    Boolean isAbyssJewel();
    Boolean isDelve();
    SocketsList getSockets();
    String getName();
    String getTypeLine();
    Boolean isDuplicated();
    boolean isIdentified();
    String getNote();
    Boolean isCorrupted();
    PropertiesList getProperties();
    PropertiesList getAdditionalProperties();
    PropertiesList getRequirements();
    PropertiesList getNextLevelRequirements();
    String secDescrText();
    ModsList getUtilityMods();
    ModsList getImplicitMods();
    ModsList getExplicitMods();
    ModsList getCraftedMods();
    ModsList getFracturedMods();
    String getDescrText();
    FlavourTextList getFlavourText();
    String getProphecyText();
    int getFrameType();
    Integer getStackSize();
    Integer getMaxStackSize();
    String getArtFilename();
    Category getCategory();
    Integer getX();
    Integer getY();
    String getInventoryId();
    SocketedItemsList getSocketedItems();
    String getColour();
}
