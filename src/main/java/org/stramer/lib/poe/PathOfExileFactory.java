/*
   Copyright 2019 Anthony Stramer <anthony@stramer.org>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */

package org.stramer.lib.poe;

import org.stramer.lib.poe.conf.Configuration;
import org.stramer.lib.poe.conf.ConfigurationContext;


//TODO Configuration can't actually be set, so yeah, fuck you... apparently?
public class PathOfExileFactory {
    private static final PathOfExile SINGLETON;
    private final Configuration conf;

    static {
        SINGLETON = new PathOfExileImpl(ConfigurationContext.getInstance());
    }

    public PathOfExileFactory() {
        this(ConfigurationContext.getInstance());
    }

    public PathOfExileFactory(Configuration conf) {
        this.conf = conf;
    }

    public PathOfExile getInstance() {
        return new PathOfExileImpl(conf);
    }

    public static PathOfExile getSingleton() {
        return SINGLETON;
    }
}
