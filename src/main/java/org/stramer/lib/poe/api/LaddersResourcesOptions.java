/*
   Copyright 2019 Anthony Stramer <anthony@stramer.org>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */

package org.stramer.lib.poe.api;

import java.util.Map;
import java.util.TreeMap;

public final class LaddersResourcesOptions implements ResourcesOptions {
    private Realm realm;
    private Integer limit;
    private Integer offset;
    private Type type;
    private Boolean track;
    private String accountName;
    private Difficulty difficulty;
    private Long start;

    public LaddersResourcesOptions() {
        realm = Realm.PC;
        limit = 20;
        offset = 0;
        type = Type.LEAGUE;
        track = true;
        accountName = null;
        difficulty = null;
        start = null;
    }

    public Realm getRealm() {
        return realm;
    }

    /**
     * The realm of the league for the ladder you want to retrieve.
     *
     * @param realm pc (default), xbox, or sony
     */
    public void setRealm(Realm realm) {
        if(realm == null) throw new IllegalArgumentException("Realm cannot be null");
        this.realm = realm;
    }

    public Integer getLimit() {
        return limit;
    }

    /**
     * Specifies the number of ladder entries to include.
     *
     * @param limit Default: 20, Max: 200.
     */
    public void setLimit(Integer limit) {
        if(limit == null || limit <= 0 || limit > 200) throw new IllegalArgumentException("Limit expects an Integer between 1 and 200");
        this.limit = limit;
    }

    public Integer getOffset() {
        return offset;
    }

    /**
     * Specifies the offset to the first ladder entry to include.
     *
     * @param offset Default 0
     */
    public void setOffset(Integer offset) {
        if(offset == null || offset < 0) throw new IllegalArgumentException("Offset expects a non-negative Integer");
        this.offset = offset;
    }

    public Type getType() {
        return type;
    }

    /**
     * Specifies the type of ladder.
     *
     * @param type league (default), pvp, labyrinth
     */
    public void setType(Type type) {
        if(type == null) throw new IllegalArgumentException("Type cannot be null");
        this.type = type;
    }

    public boolean getTrack() {
        return track;
    }

    /**
     * Adds unique IDs for each character returned. These can be used when name conflicts occur.
     *
     * @param track Default: true
     */
    public void setTrack(boolean track) {
        this.track = track;
    }

    public String getAccountName() {
        return accountName;
    }

    /**
     * League only: Filters by account name within the first 15000 results.
     *
     * @param accountName Account name
     */
    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public Difficulty getDifficulty() {
        return difficulty;
    }

    /**
     * Labyrinth only: Normal (1), Cruel (2), or Merciless (3)
     *
     * @param difficulty Difficulty
     */
    public void setDifficulty(Difficulty difficulty) {
        this.difficulty = difficulty;
    }

    public Long getStart() {
        return start;
    }

    /**
     * Labyrinth only: Timestamp of the ladder you want.
     *
     * @param start Timestamp
     */
    public void setStart(Long start) {
        if(start < 0L) throw new IllegalArgumentException("Start expects a non-negative Long");
        this.start = start;
    }

    @Override
    public Map<String, String> toMap() {
        Map<String, String> map = new TreeMap<>();

        map.put("realm", realm.toString().toLowerCase());
        map.put("limit", limit.toString());
        map.put("offset", offset.toString());
        map.put("type", type.toString().toLowerCase());
        map.put("track", track.toString());

        // League Only Options
        if(type.equals(Type.LEAGUE) && accountName != null) {
            map.put("accountName", accountName);
        }

        // Labyrinth Only Options
        if(type.equals(Type.LABYRINTH)) {
            if(difficulty != null) map.put("difficulty", difficulty.getId().toString());
            if(start != null) map.put("start", start.toString());
        }

        return map;
    }

    @Override
    public String toString() {
        return "LaddersResourcesOptions{" +
                "realm='" + realm + '\'' +
                ", limit=" + limit +
                ", offset=" + offset +
                ", type='" + type + '\'' +
                ", track=" + track +
                ", accountName='" + accountName + '\'' +
                ", difficulty=" + difficulty +
                ", start=" + start +
                '}';
    }

    public enum Difficulty {NORMAL(1), CRUEL(2), MERCILESS(3);
        private Integer id;

        Difficulty(int id) {
            this.id = id;
        }

        public Integer getId() {
            return id;
        }
    }
    public enum Type {LEAGUE, PVP, LABYRINTH}
}
