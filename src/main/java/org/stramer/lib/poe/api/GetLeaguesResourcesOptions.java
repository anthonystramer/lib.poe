/*
   Copyright 2019 Anthony Stramer <anthony@stramer.org>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */

package org.stramer.lib.poe.api;

import java.util.Map;
import java.util.TreeMap;

public final class GetLeaguesResourcesOptions implements ResourcesOptions {
    private Realm realm;
    private boolean ladder;
    private int ladderLimit;
    private int ladderOffset;
    private boolean ladderTrack;

    public GetLeaguesResourcesOptions() {
        realm = Realm.PC;
        ladder = false;
        ladderLimit = 20;
        ladderOffset = 0;
        ladderTrack = false;
    }

    public Realm getRealm() {
        return realm;
    }

    /**
     * The realm of the leagues.
     *
     * @param realm pc (default), xbox, or sony
     */
    public void setRealm(Realm realm) {
        if(realm == null) throw new IllegalArgumentException("Realm cannot be null");
        this.realm = realm;
    }

    public boolean isLadder() {
        return ladder;
    }

    /**
     * Set to true to include the ladder in the response. The ladder will be in included in the "ladder" key. Defaults to false, excluding the ladder. Please note that the ladder can be retrieved using the ladder resource, and that retrieving it using the league API is just an optimization that can be used if you are requesting the league anyway.
     *
     * @param ladder true for ladder in response
     */
    public void setLadder(boolean ladder) {
        this.ladder = ladder;
    }

    public int getLadderLimit() {
        return ladderLimit;
    }

    /**
     * When including the ladder with ladder=true, this specifies the number of ladder entries to include.
     *
     * @param ladderLimit Default: 20, Max: 200
     */
    public void setLadderLimit(int ladderLimit) {
        if(ladderLimit < 1 || ladderLimit > 200) throw new IllegalArgumentException("Ladder limit expects a non-negative Integer between 1 and 200");
        this.ladderLimit = ladderLimit;
    }

    public int getLadderOffset() {
        return ladderOffset;
    }

    /**
     * When including the ladder with ladder=true, this specifies the offset to the first ladder entry to include.
     *
     * @param ladderOffset Default: 0
     */
    public void setLadderOffset(int ladderOffset) {
        if(ladderOffset < 0) throw new IllegalArgumentException("Ladder offset expects a non-negative Integer");
        this.ladderOffset = ladderOffset;
    }

    public boolean isLadderTrack() {
        return ladderTrack;
    }

    /**
     * When including the ladder with ladder=true, this setting adds unique IDs for each character returned. These can be used when name conflicts occur.
     *
     * @param ladderTrack true for unique IDs
     */
    public void setLadderTrack(boolean ladderTrack) {
        this.ladderTrack = ladderTrack;
    }

    @Override
    public Map<String, String> toMap() {
        Map<String, String> map = new TreeMap<>();

        map.put("realm", realm.toString().toLowerCase());
        map.put("ladder", ladder ? "1" : "0");

        if(ladder) {
            map.put("ladderLimit", Integer.valueOf(ladderLimit).toString());
            map.put("ladderOffset", Integer.valueOf(ladderOffset).toString());
            map.put("ladderTrack", ladderTrack ? "1" : "0");
        }

        return map;
    }

    @Override
    public String toString() {
        return "GetLeaguesResourcesOptions{" +
                "realm=" + realm +
                ", ladder=" + ladder +
                ", ladderLimit=" + ladderLimit +
                ", ladderOffset=" + ladderOffset +
                ", ladderTrack=" + ladderTrack +
                '}';
    }
}
