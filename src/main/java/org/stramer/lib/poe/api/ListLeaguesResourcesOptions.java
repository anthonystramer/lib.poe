/*
   Copyright 2019 Anthony Stramer <anthony@stramer.org>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */

package org.stramer.lib.poe.api;

import java.util.Map;
import java.util.TreeMap;

public final class ListLeaguesResourcesOptions implements ResourcesOptions {
    private Type type;
    private Realm realm;
    private String season;
    private boolean compact;
    private Integer limit;
    private int offset;

    public ListLeaguesResourcesOptions() {
        type = Type.MAIN;
        realm = Realm.PC;
        season = null;
        compact = false;
        limit = null;
        offset = 0;
    }

    public Type getType() {
        return type;
    }

    /**
     * Specifies the league type.
     * - "main": retrieves permanent and challenge leagues (default)
     * - "event": retrieves event leagues
     * - "season": retrieves leagues in a particular season.
     *
     * @param type main (default), event, or season
     */
    public void setType(Type type) {
        if(type == null) throw new IllegalArgumentException("Type cannot be null");
        this.type = type;
    }

    public Realm getRealm() {
        return realm;
    }

    /**
     * The realm of the leagues.
     *
     * @param realm pc (default), xbox, or sony
     */
    public void setRealm(Realm realm) {
        if(realm == null) throw new IllegalArgumentException("Realm cannot be null");
        this.realm = realm;
    }

    public String getSeason() {
        return season;
    }

    /**
     * A particular season id. Required when type=season.
     *
     * @param season Season id
     */
    public void setSeason(String season) {
        this.season = season;
    }

    public boolean isCompact() {
        return compact;
    }

    /**
     * Set if the retrieval should be compacted.
     * - false: Displays the full info for leagues retrieved (will only retrieve a maximum of 50 leagues) (the default)
     * - true: Display compact info for leagues retrieved (will retrieve up to 230 leagues).
     *
     * @param compact false (default) or true
     */
    public void setCompact(boolean compact) {
        this.compact = compact;
    }

    public Integer getLimit() {
        return limit;
    }

    /**
     * This specifies the number of league entries to include. By default this is the maximum, which depends on the setting above.
     *
     * @param limit Integer
     */
    public void setLimit(Integer limit) {
        if(limit != null && limit < 0) throw new IllegalArgumentException("Limit expects a non-negative Integer");
        this.limit = limit;
    }

    public int getOffset() {
        return offset;
    }

    /**
     * This specifies the offset to the first league entry to include.
     *
     * @param offset Default: 0
     */
    public void setOffset(int offset) {
        if(offset < 0) throw new IllegalArgumentException("Offset expects a non-negative Integer");
        this.offset = offset;
    }

    @Override
    public Map<String, String> toMap() {
        Map<String, String> map = new TreeMap<>();
        map.put("type", type.toString().toLowerCase());
        map.put("realm", realm.toString().toLowerCase());
        map.put("compact", compact ? "1" : "0");
        map.put("offset", Integer.valueOf(offset).toString());

        if(limit != null) map.put("limit", limit.toString());
        if(type.equals(Type.SEASON)) map.put("season", season);

        return map;
    }

    @Override
    public String toString() {
        return "ListLeaguesResourcesOptions{" +
                "type=" + type +
                ", realm=" + realm +
                ", season='" + season + '\'' +
                ", compact=" + compact +
                ", limit=" + limit +
                ", offset=" + offset +
                '}';
    }

    public enum Type {MAIN, EVENT, SEASON}
}
