/*
   Copyright 2019 Anthony Stramer <anthony@stramer.org>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */

package org.stramer.lib.poe.api;

import java.util.Map;
import java.util.TreeMap;

public final class PvPMatchesResourcesOptions implements ResourcesOptions {
    private String seasonId;
    private Realm realm;

    public PvPMatchesResourcesOptions() {
        seasonId = null;
        realm = Realm.PC;
    }

    public String getSeasonId() {
        return seasonId;
    }

    /**
     * Set this to get PvP matches for a particular season. Leave this unset to retrieve all upcoming PvP matches.
     * @param seasonId Season id, null for upcoming matches
     */
    public void setSeasonId(String seasonId) {
        this.seasonId = seasonId;
    }

    public Realm getRealm() {
        return realm;
    }

    /**
     * The realm to fetch PvP matches from: pc (default), xbox, or sony.
     * @param realm pc (default), xbox, or sony
     */
    public void setRealm(Realm realm) {
        if(realm == null) throw new IllegalArgumentException("Realm cannot be null");
        this.realm = realm;
    }

    @Override
    public Map<String, String> toMap() {
        Map<String, String> map = new TreeMap<>();
        if(seasonId != null) map.put("seasonId", seasonId);
        map.put("realm", realm.toString().toLowerCase());
        return map;
    }

    @Override
    public String toString() {
        return "PvPMatchesResourcesOptions{" +
                "seasonId='" + seasonId + '\'' +
                ", realm=" + realm +
                '}';
    }
}
