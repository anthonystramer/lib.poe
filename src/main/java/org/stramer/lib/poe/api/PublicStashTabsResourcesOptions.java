/*
   Copyright 2019 Anthony Stramer <anthony@stramer.org>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */

package org.stramer.lib.poe.api;

import java.util.Map;
import java.util.TreeMap;

public final class PublicStashTabsResourcesOptions implements ResourcesOptions {
    private String id;

    public PublicStashTabsResourcesOptions() {
        id = "0";
    }

    public String getId() {
        return id;
    }

    /**
     * The next change ID you received from previously fetching changes.
     * @param id Default: 0
     */
    public void setId(String id) {
        if(id == null) throw new IllegalArgumentException("Id cannot be null");
        this.id = id;
    }

    @Override
    public Map<String, String> toMap() {
        Map<String, String> map = new TreeMap<>();
        map.put("id", id);
        return map;
    }

    @Override
    public String toString() {
        return "PublicStashTabsResourcesOptions{" +
                "id='" + id + '\'' +
                '}';
    }
}
