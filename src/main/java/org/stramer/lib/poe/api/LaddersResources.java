/*
   Copyright 2019 Anthony Stramer <anthony@stramer.org>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */

package org.stramer.lib.poe.api;

import org.stramer.lib.poe.LaddersList;
import org.stramer.lib.poe.PathOfExileException;

public interface LaddersResources {
    /**
     * Get a ladder by league id.
     *
     * @param id League id (name)
     * @return List
     * @throws PathOfExileException
     * @see <a href="https://www.pathofexile.com/developer/docs/api-resource-ladders">GET /ladders/:id - Path of Exile API Resources</a>
     */
    LaddersList getLadders(String id)
            throws PathOfExileException;

    /**
     * Get a ladder by league id.
     *
     * @param id League id (name)
     * @param options Optional request parameters
     * @return List
     * @throws PathOfExileException
     * @see <a href="https://www.pathofexile.com/developer/docs/api-resource-ladders">GET /ladders/:id - Path of Exile API Resources</a>
     */
    LaddersList getLadders(String id, LaddersResourcesOptions options)
            throws PathOfExileException;
}
