/*
   Copyright 2019 Anthony Stramer <anthony@stramer.org>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */

package org.stramer.lib.poe.api;

import org.stramer.lib.poe.PathOfExileException;
import org.stramer.lib.poe.PvPMatchesList;

public interface PvPMatchesResources {
    /**
     * Get a list of PvP matches.
     *
     * @return List
     * @throws PathOfExileException
     * @see <a href="https://www.pathofexile.com/developer/docs/api-resource-pvp-matches#list">GET /pvp-matches - Path of Exile API Resources</a>
     */
    PvPMatchesList getPvPMatches()
        throws PathOfExileException;

    /**
     * Get a list of PvP matches.
     *
     * @param options Request options
     * @return List
     * @throws PathOfExileException
     * @see <a href="https://www.pathofexile.com/developer/docs/api-resource-pvp-matches#list">GET /pvp-matches - Path of Exile API Resources</a>
     */
    PvPMatchesList getPvPMatches(PvPMatchesResourcesOptions options)
            throws PathOfExileException;
}
