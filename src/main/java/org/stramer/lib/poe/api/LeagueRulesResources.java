/*
   Copyright 2019 Anthony Stramer <anthony@stramer.org>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */

package org.stramer.lib.poe.api;

import org.stramer.lib.poe.LeagueRule;
import org.stramer.lib.poe.LeagueRulesList;
import org.stramer.lib.poe.PathOfExileException;

public interface LeagueRulesResources {

    /**
     * Get a list of all possible league rules.
     *
     * @return List
     * @throws PathOfExileException
     * @see <a href="https://www.pathofexile.com/developer/docs/api-resource-league-rules#list">GET /league-rules - Path of Exile API Resources</a>
     */
    LeagueRulesList getLeagueRules()
        throws PathOfExileException;

    /**
     * Get a single league rule by id.
     *
     * @param id Rule id
     * @return LeagueRule
     * @throws PathOfExileException
     * @see <a href="https://www.pathofexile.com/developer/docs/api-resource-league-rules#get">GET /league-rules/:id - Path of Exile API Resources</a>
     */
    LeagueRule getLeagueRule(String id)
        throws PathOfExileException;
}
