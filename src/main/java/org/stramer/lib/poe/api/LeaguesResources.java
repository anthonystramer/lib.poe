/*
   Copyright 2019 Anthony Stramer <anthony@stramer.org>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */

package org.stramer.lib.poe.api;

import org.stramer.lib.poe.League;
import org.stramer.lib.poe.LeaguesList;
import org.stramer.lib.poe.PathOfExileException;

public interface LeaguesResources {
    /**
     * Get a list of current and past leagues.
     *
     * @return List
     * @throws PathOfExileException
     * @see <a href="https://www.pathofexile.com/developer/docs/api-resource-leagues#list">GET /leagues - Path of Exile API Resources</a>
     */
    LeaguesList getLeagues()
        throws PathOfExileException;

    /**
     * Get a list of current and past leagues.
     *
     * @param options Request options
     * @return List
     * @throws PathOfExileException
     * @see <a href="https://www.pathofexile.com/developer/docs/api-resource-leagues#list">GET /leagues - Path of Exile API Resources</a>
     */
    LeaguesList getLeagues(ListLeaguesResourcesOptions options)
            throws PathOfExileException;

    /**
     * Get a single league by id.
     *
     * @param id League id
     * @return League
     * @throws PathOfExileException
     * @see <a href="https://www.pathofexile.com/developer/docs/api-resource-leagues#get">GET /leagues/:id - Path of Exile API Resources</a>
     */
    League getLeague(String id)
        throws PathOfExileException;

    /**
     * Get a single league by id.
     *
     * @param id League id
     * @param options Request options
     * @return League
     * @throws PathOfExileException
     * @see <a href="https://www.pathofexile.com/developer/docs/api-resource-leagues#get">GET /leagues/:id - Path of Exile API Resources</a>
     */
    League getLeague(String id, GetLeaguesResourcesOptions options)
            throws PathOfExileException;
}
