/*
   Copyright 2019 Anthony Stramer <anthony@stramer.org>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */

package org.stramer.lib.poe.conf;

import org.stramer.lib.poe.Logger;

import java.lang.reflect.Field;
import java.util.Objects;

public class ConfigurationBase implements Configuration {
    private String baseURL = "https://api.pathofexile.com";
    private ProcessingStrictness processingStrictness = ProcessingStrictness.LOOSE;
    private String loggerFactory = null;
    private boolean debug = false;

    @Override
    public String getBaseURL() {
        return baseURL;
    }

    protected void setBaseURL(String url) {
        this.baseURL = url;
    }

    @Override
    public ProcessingStrictness getProcessingStrictness() {
        return processingStrictness;
    }

    protected void setProcessingStrictness(ProcessingStrictness processingStrictness) {
        this.processingStrictness = processingStrictness;
    }

    @Override
    public String getLoggerFactory() {
        return loggerFactory;
    }

    protected void setLoggerFactory(String loggerFactory) {
        this.loggerFactory = loggerFactory;
    }

    @Override
    public boolean isDebugEnabled() {
        return debug;
    }

    protected void setDebug(boolean debug) {
        this.debug = debug;
    }

    public void dumpConfiguration() {
        Logger logger = Logger.getLogger(ConfigurationBase.class);

        if(debug) {
            Field[] fields = Configuration.class.getDeclaredFields();
            for(Field field : fields) {
                try {
                    Object value = field.get(this);
                    String strValue = String.valueOf(value);
                    logger.debug(field.getName() + ": " + strValue);
                } catch (IllegalAccessException ignore) {
                }
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || !ConfigurationBase.class.isAssignableFrom(o.getClass())) return false;
        ConfigurationBase base = (ConfigurationBase) o;
        return debug == base.debug &&
                baseURL.equals(base.baseURL) &&
                processingStrictness == base.processingStrictness &&
                Objects.equals(loggerFactory, base.loggerFactory);
    }

    @Override
    public int hashCode() {
        return Objects.hash(baseURL, processingStrictness, loggerFactory, debug);
    }

    @Override
    public String toString() {
        return "ConfigurationBase{" +
                "baseURL='" + baseURL + '\'' +
                ", processingStrictness=" + processingStrictness +
                ", loggerFactory='" + loggerFactory + '\'' +
                ", debug=" + debug +
                '}';
    }
}
