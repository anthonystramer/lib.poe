/*
   Copyright 2019 Anthony Stramer <anthony@stramer.org>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */

package org.stramer.lib.poe.conf;

import java.net.URL;

// TODO rebuild this class so that each method controls its own configuration to base
public class ConfigurationBuilder {
    private URL baseURL;
    private ProcessingStrictness processingStrictness;
    private String loggerFactory;
    private Boolean debug;

    public ConfigurationBuilder setBaseUrl(URL url) {
        this.baseURL = url;

        return this;
    }

    public ConfigurationBuilder setProcessingStrictness(ProcessingStrictness processingStrictness) {
        this.processingStrictness = processingStrictness;

        return this;
    }

    public ConfigurationBuilder setLoggerFactory(String loggerFactory) {
        this.loggerFactory = loggerFactory;

        return this;
    }

    public ConfigurationBuilder setDebug(boolean debug) {
        this.debug = debug;

        return this;
    }

    public Configuration build() {
        CustomConfiguration configuration = new CustomConfiguration();

        if(baseURL != null) configuration.setBaseURL(baseURL.toString());
        if(processingStrictness != null) configuration.setProcessingStrictness(processingStrictness);
        if(loggerFactory != null) configuration.setLoggerFactory(loggerFactory);
        if(debug != null) configuration.setDebug(debug);

        return configuration;
    }

    @Override
    public String toString() {
        return "ConfigurationBuilder{" +
                "baseURL=" + baseURL +
                ", processingStrictness=" + processingStrictness +
                ", loggerFactory='" + loggerFactory + '\'' +
                ", debug=" + debug +
                '}';
    }
}
