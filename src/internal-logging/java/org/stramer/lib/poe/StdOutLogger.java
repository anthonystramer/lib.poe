/*
   Copyright 2019 Anthony Stramer <anthony@stramer.org>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */

package org.stramer.lib.poe;

import org.stramer.lib.poe.conf.ConfigurationContext;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

// TODO: update this to actually identify what severity level is being sent, also probably a good idea to show the thread too
final class StdOutLogger extends Logger {
    private static final boolean DEBUG = ConfigurationContext.getInstance().isDebugEnabled();
    private static final DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mmZ");

    @Override
    public boolean isDebugEnabled() {
        return DEBUG;
    }

    @Override
    public boolean isInfoEnabled() {
        return true;
    }

    @Override
    public boolean isWarnEnabled() {
        return true;
    }

    @Override
    public boolean isErrorEnabled() {
        return true;
    }

    @Override
    public void debug(String message) {
        if(DEBUG) {
            println("DEBUG", message);
        }
    }

    @Override
    public void info(String message) {
        println("INFO", message);
    }

    @Override
    public void warn(String message) {
        println("WARN", message);
    }

    @Override
    public void warn(String message, Throwable th) {
        println("WARN", message);
        th.printStackTrace(System.err); // TODO don't know if I like this, feels hacky to print to stdout then push stack traces to stderr
    }

    @Override
    public void error(String message) {
        println("ERROR", message);
    }

    @Override
    public void error(String message, Throwable th) {
        println("ERROR", message);
        th.printStackTrace(System.err); // TODO don't know if I like this, feels hacky to print to stdout then push stack traces to stderr
    }

    private void println(String type, String message) {
        System.out.printf("%s %-5s [%s] %s\n", df.format(new Date()), type, Thread.currentThread().getName(), message);
    }
}
