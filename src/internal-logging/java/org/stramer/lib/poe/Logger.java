/*
   Copyright 2019 Anthony Stramer <anthony@stramer.org>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */

package org.stramer.lib.poe;

import org.stramer.lib.poe.conf.ConfigurationBase;
import org.stramer.lib.poe.conf.ConfigurationContext;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public abstract class Logger {
    private static LoggerFactory LOGGER_FACTORY;
    private static final String LOGGER_FACTORY_IMPLEMENTATION = "org.stramer.lib.poe.loggerFactory";
    private static final String LOGGER_FACTORY_IMPLEMENTATION_ENV = "org_stramer_lib_poe_loggerFactory";

    private static LoggerFactory getLoggerFactoryIfAvailable(String checkClassName, String implementationClass) {
        try {
            Class.forName(checkClassName);
            return (LoggerFactory) Class.forName(implementationClass).getDeclaredConstructor().newInstance();
        } catch (ClassNotFoundException | NoSuchMethodException | InvocationTargetException ignore) {
            // Logging is optional, so we're really only trying to make best effort
        } catch (SecurityException ignore) {
            // Unsigned applets are not allowed to access System properties, but we really don't care
        } catch (InstantiationException | IllegalAccessException e) {
            throw new AssertionError(e);
        }

        return null;
    }

    public static synchronized Logger getLogger(Class<?> clazz) {
        if(LOGGER_FACTORY == null) {
            // Priority:
            //   Java Property -> Environment Property -> Configuration -> log4j -> stdout
            LoggerFactory loggerFactory = null;

            // -Dlog.poe.loggerFactory=log.poe.StdOutLoggerFactory
            String loggerFactoryImpl = System.getProperty(LOGGER_FACTORY_IMPLEMENTATION);

            if(loggerFactoryImpl == null) {
                loggerFactoryImpl = System.getenv(LOGGER_FACTORY_IMPLEMENTATION_ENV);
            }

            if(loggerFactoryImpl != null) {
                loggerFactory = getLoggerFactoryIfAvailable(loggerFactoryImpl, loggerFactoryImpl);
            }

            ConfigurationBase configuration = (ConfigurationBase)ConfigurationContext.getInstance();
            loggerFactoryImpl = configuration.getLoggerFactory();
            if(loggerFactory == null && loggerFactoryImpl != null) {
                loggerFactory = getLoggerFactoryIfAvailable(loggerFactoryImpl, loggerFactoryImpl);
            }

            if(loggerFactory == null) {
                loggerFactory = getLoggerFactoryIfAvailable("org.apache.log4j.Logger", "org.stramer.lib.poe.Log4JLoggerFactory");
            }

            if(loggerFactory == null) {
                loggerFactory = new StdOutLoggerFactory();
            }

            LOGGER_FACTORY = loggerFactory;

            try {
                // TODO yeah this doesn't actually work
                Method method = configuration.getClass().getMethod("dumpConfiguration");
                method.setAccessible(true);
                method.invoke(configuration);
            } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException ignore) {
            }
        }

        return LOGGER_FACTORY.getLogger(clazz);
    }

    public abstract boolean isDebugEnabled();

    public abstract boolean isInfoEnabled();

    public abstract boolean isWarnEnabled();

    public abstract boolean isErrorEnabled();

    public abstract void debug(String message);

    public abstract void info(String message);

    public abstract void warn(String message);

    public abstract void warn(String message, Throwable th);

    public abstract void error(String message);

    public abstract void error(String message, Throwable th);
}
