/*
   Copyright 2019 Anthony Stramer <anthony@stramer.org>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */

package org.stramer.lib.poe;

import com.google.gson.GsonBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class ItemImplTest extends UnitTest {

    @BeforeEach
    void setup() {
        gson = new GsonBuilder()
                .registerTypeAdapter(ItemImpl.class, new ItemImpl(configuration))
                .registerTypeAdapter(Item.class, new ItemImpl(configuration))
                .registerTypeAdapter(Category.class, new CategoryImpl(configuration))
                .registerTypeAdapter(ModsList.class, new ModsListImpl())
                .registerTypeAdapter(FlavourTextList.class, new FlavourTextListImpl())
                .registerTypeAdapter(Property.class, new PropertyImpl(configuration))
                .registerTypeAdapter(PropertiesList.class, new PropertiesListImpl())
                .registerTypeAdapter(Socket.class, new SocketImpl(configuration))
                .registerTypeAdapter(SocketsList.class, new SocketsListImpl())
                .registerTypeAdapter(SocketedItemsList.class, new SocketedItemsListImpl())
                .registerTypeAdapter(Value.class, new ValueImpl(configuration))
                .registerTypeAdapter(ValuesList.class, new ValuesListImpl())
                .create();
    }

    @Test
    void categoryAccessoriesRing() throws FileNotFoundException, MalformedURLException {
        String json = getTestJson("itemImpl/category_accessories_ring.json");

        ItemImpl item = gson.fromJson(json, ItemImpl.class);

        assertFalse(item.isVerified());
        assertEquals(1, item.getW());
        assertEquals(1, item.getH());
        assertEquals(75, item.getIlvl());
        assertEquals(new URL("https://web.poecdn.com/image/Art/2DItems/Rings/Ring3Unique.png?scale=1&w=1&h=1&v=689dcd2e6f55af30ca0fa8e1f795e807"), item.getIcon());
        assertEquals("Hardcore Synthesis", item.getLeague());
        assertEquals("46467d2b63dfb36bbc99eef4ebb776d55195f8dbd1d1072e12fe1872774e3db6", item.getId());
        assertEquals("Doedre's Damning", item.getName());
        assertEquals("Paua Ring", item.getTypeLine());
        assertTrue(item.isIdentified());
        assertEquals("~b/o 10 chaos", item.getNote());
        assertTrue(item.isCorrupted());
        assertEquals(1, item.getRequirements().size());
        assertEquals(1, item.getImplicitMods().size());
        assertEquals(4, item.getExplicitMods().size());
        assertEquals(2, item.getFlavourText().size());
        assertEquals(3, item.getFrameType());
        assertEquals(1, item.getCategory().getAccessories().size());
        assertNull(item.getCategory().getArmour());
        assertNull(item.getCategory().getWeapons());
        assertNull(item.getCategory().getJewels());
        assertNull(item.getCategory().getCards());
        assertNull(item.getCategory().getCurrency());
        assertNull(item.getCategory().getGems());
        assertEquals("ring", item.getCategory().getAccessories().get(0));
        assertEquals(9, (int)item.getX());
        assertEquals(9, (int)item.getY());
        assertEquals("Stash11", item.getInventoryId());
    }

    @Test
    void categoryArmourChest() throws FileNotFoundException, MalformedURLException {
        String json = getTestJson("itemImpl/category_armour_chest.json");

        ItemImpl item = gson.fromJson(json, ItemImpl.class);

        assertFalse(item.isVerified());
        assertEquals(2, item.getW());
        assertEquals(3, item.getH());
        assertEquals(81, item.getIlvl());
        assertEquals(new URL("https://web.poecdn.com/image/Art/2DItems/Armours/BodyArmours/SoulMantle.png?scale=1&w=2&h=3&v=f224ff8c682cc73d339de197515b7738"), item.getIcon());
        assertEquals("Synthesis", item.getLeague());
        assertEquals(1, item.getSockets().size());
        assertEquals("Soul Mantle", item.getName());
        assertEquals("Spidersilk Robe", item.getTypeLine());
        assertTrue(item.isIdentified());
        assertTrue(item.isCorrupted());
        assertEquals(1, item.getProperties().size());
        assertEquals(2, item.getRequirements().size());
        assertEquals(1, item.getImplicitMods().size());
        assertEquals(6, item.getExplicitMods().size());
        assertEquals(2, item.getFlavourText().size());
        assertEquals(3, item.getFrameType());
        assertEquals(1, item.getCategory().getArmour().size());
        assertNull(item.getCategory().getAccessories());
        assertNull(item.getCategory().getWeapons());
        assertNull(item.getCategory().getJewels());
        assertNull(item.getCategory().getCards());
        assertNull(item.getCategory().getCurrency());
        assertNull(item.getCategory().getGems());
        assertEquals("chest", item.getCategory().getArmour().get(0));
        assertEquals(9, (int)item.getX());
        assertEquals(0, (int)item.getY());
        assertEquals(0, item.getSocketedItems().size());
    }

    @Test
    void categoryCards() throws FileNotFoundException, MalformedURLException {
        String json = getTestJson("itemImpl/category_cards.json");

        ItemImpl item = gson.fromJson(json, ItemImpl.class);

        assertFalse(item.isVerified());
        assertEquals(1, item.getW());
        assertEquals(1, item.getH());
        assertEquals(0, item.getIlvl());
        assertEquals(new URL("https://web.poecdn.com/image/Art/2DItems/Divination/InventoryIcon.png?scale=1&w=1&h=1&v=a8ae131b97fad3c64de0e6d9f250d743"), item.getIcon());
        assertEquals("Synthesis", item.getLeague());
        assertEquals("6b40db2e6f22de44bf162dbfaf3f69c450e8c93a49c785f47495a7cce5c1c403", item.getId());
        assertEquals("", item.getName());
        assertEquals("Boon of the First Ones", item.getTypeLine());
        assertTrue(item.isIdentified());
        assertEquals(1, item.getProperties().size());
        assertEquals(1, item.getExplicitMods().size());
        assertEquals(4, item.getFlavourText().size());
        assertEquals(6, item.getFrameType());
        assertEquals(2, (int)item.getStackSize());
        assertEquals(6, (int)item.getMaxStackSize());
        assertEquals("BoonoftheFirstOnes", item.getArtFilename());
        assertEquals(0, item.getCategory().getCards().size());
        assertNull(item.getCategory().getArmour());
        assertNull(item.getCategory().getWeapons());
        assertNull(item.getCategory().getJewels());
        assertNull(item.getCategory().getAccessories());
        assertNull(item.getCategory().getCurrency());
        assertNull(item.getCategory().getGems());
        assertEquals(228, (int)item.getX());
        assertEquals(0, (int)item.getY());
        assertEquals("Stash46", item.getInventoryId());

    }

    @Test
    void currencyBlacksmithsWhetstone() throws FileNotFoundException, MalformedURLException {
        String json = getTestJson("itemImpl/category_currency_blacksmiths-whetstone.json");

        ItemImpl item = gson.fromJson(json, ItemImpl.class);

        assertFalse(item.isVerified());
        assertEquals(1, item.getW());
        assertEquals(1, item.getH());
        assertEquals(0, item.getIlvl());
        assertEquals(new URL("https://web.poecdn.com/image/Art/2DItems/Currency/CurrencyWeaponQuality.png?scale=1&w=1&h=1&v=d2ce9167e23a74cef5d8465433e86482"), item.getIcon());
        assertEquals("Synthesis", item.getLeague());
        assertEquals("c1a977d6bf062297d6a25ef742c459c25000235d1e27c0d6a9390b5955067314", item.getId());
        assertEquals("", item.getName());
        assertEquals("Blacksmith's Whetstone", item.getTypeLine());
        assertTrue(item.isIdentified());
        assertEquals(1, item.getProperties().size());
        assertEquals(1, item.getExplicitMods().size());
        assertEquals("Right click this item then left click a weapon to apply it. Has greater effect on lower rarity weapons. The maximum quality is 20%.", item.getDescrText());
        assertEquals(5, item.getFrameType());
        assertEquals(203, (int)item.getStackSize());
        assertEquals(5000, (int)item.getMaxStackSize());
        assertEquals(0, item.getCategory().getCurrency().size());
        assertNull(item.getCategory().getArmour());
        assertNull(item.getCategory().getWeapons());
        assertNull(item.getCategory().getJewels());
        assertNull(item.getCategory().getAccessories());
        assertNull(item.getCategory().getCards());
        assertNull(item.getCategory().getGems());
        assertEquals(0, (int)item.getX());
        assertEquals(0, (int)item.getY());
        assertEquals("Stash35", item.getInventoryId());

    }

    @Test
    void currencyEssence() throws FileNotFoundException, MalformedURLException {
        String json = getTestJson("itemImpl/category_currency_essence.json");

        ItemImpl item = gson.fromJson(json, ItemImpl.class);

        assertFalse(item.isVerified());
        assertEquals(1, item.getW());
        assertEquals(1, item.getH());
        assertEquals(0, item.getIlvl());
        assertEquals(new URL("https://web.poecdn.com/image/Art/2DItems/Currency/Essence/Greed5.png?scale=1&w=1&h=1&v=f2cac5eb3527123b27ef94c99edbe3a2"), item.getIcon());
        assertEquals("Synthesis", item.getLeague());
        assertEquals("c726a58ae4fdf5b4a540eb32720f101e40e78c0850c69ba99a376045014b9923", item.getId());
        assertEquals("", item.getName());
        assertEquals("Screaming Essence of Greed", item.getTypeLine());
        assertTrue(item.isIdentified());
        assertEquals(1, item.getProperties().size());
        assertEquals(10, item.getExplicitMods().size());
        assertEquals("Right click this item then left click a normal item to apply it.", item.getDescrText());
        assertEquals(5, item.getFrameType());
        assertEquals(2, (int)item.getStackSize());
        assertEquals(9, (int)item.getMaxStackSize());
        assertEquals(0, item.getCategory().getCurrency().size());
        assertNull(item.getCategory().getArmour());
        assertNull(item.getCategory().getWeapons());
        assertNull(item.getCategory().getJewels());
        assertNull(item.getCategory().getAccessories());
        assertNull(item.getCategory().getCards());
        assertNull(item.getCategory().getGems());
        assertEquals(5, (int)item.getX());
        assertEquals(14, (int)item.getY());
        assertEquals("Stash49", item.getInventoryId());

    }

    @Test
    void currencyProphecy() throws FileNotFoundException, MalformedURLException {
        String json = getTestJson("itemImpl/category_currency_prophecy.json");

        ItemImpl item = gson.fromJson(json, ItemImpl.class);

        assertFalse(item.isVerified());
        assertEquals(1, item.getW());
        assertEquals(1, item.getH());
        assertEquals(0, item.getIlvl());
        assertEquals(new URL("https://web.poecdn.com/image/Art/2DItems/Currency/ProphecyOrbRed.png?scale=1&w=1&h=1&v=dc9105d2b038a79c7c316fc2ba30cef0"), item.getIcon());
        assertEquals("Hardcore Synthesis", item.getLeague());
        assertEquals("1269a6848e0dea5fed73a279907370c15bc01ec85b2e7c838cbbf86d9a92d1b9", item.getId());
        assertEquals("", item.getName());
        assertEquals("A Master Seeks Help", item.getTypeLine());
        assertTrue(item.isIdentified());
        assertEquals("~b/o 8 chaos", item.getNote());
        assertEquals("Right-click to add this prophecy to your character.", item.getDescrText());
        assertEquals(1, item.getFlavourText().size());
        assertEquals("You will find Zana and complete her mission.", item.getProphecyText());
        assertEquals(8, item.getFrameType());
        assertEquals(0, item.getCategory().getCurrency().size());
        assertNull(item.getCategory().getArmour());
        assertNull(item.getCategory().getWeapons());
        assertNull(item.getCategory().getJewels());
        assertNull(item.getCategory().getAccessories());
        assertNull(item.getCategory().getCards());
        assertNull(item.getCategory().getGems());
        assertEquals(0, (int)item.getX());
        assertEquals(7, (int)item.getY());
        assertEquals("Stash27", item.getInventoryId());
    }

    @Test
    void currencyResonator() throws FileNotFoundException, MalformedURLException {
        String json = getTestJson("itemImpl/category_currency_resonator.json");

        ItemImpl item = gson.fromJson(json, ItemImpl.class);

        assertTrue(item.isVerified());
        assertEquals(2, item.getW());
        assertEquals(3, item.getH());
        assertEquals(10, item.getIlvl());
        assertEquals(new URL("https://web.poecdn.com/image/Art/2DItems/Currency/Delve/Reroll1x1A.png?scale=1&w=1&h=1&v=e1d44f3d008b43923e17788a3964ea5a"), item.getIcon());
        assertEquals("Synthesis", item.getLeague());
        assertEquals("248e0df6fa38d45db5944c12facefa59b1988da6f13ca59d523132e171a0678b", item.getId());
        assertTrue(item.isDelve());
        assertEquals(1, item.getSockets().size());
        assertEquals("", item.getName());
        assertEquals("Primitive Chaotic Resonator", item.getTypeLine());
        assertFalse(item.isIdentified());
        assertEquals(1, item.getProperties().size());
        assertEquals(1, item.getExplicitMods().size());
        assertEquals("All sockets must be filled with Fossils before this item can be used.", item.getDescrText());
        assertEquals(5, item.getFrameType());
        assertEquals(1, item.getCategory().getCurrency().size());
        assertEquals("resonator", item.getCategory().getCurrency().get(0));
        assertNull(item.getCategory().getArmour());
        assertNull(item.getCategory().getWeapons());
        assertNull(item.getCategory().getJewels());
        assertNull(item.getCategory().getAccessories());
        assertNull(item.getCategory().getCards());
        assertNull(item.getCategory().getGems());
        assertEquals(4, (int)item.getX());
        assertEquals(6, (int)item.getY());
        assertEquals("Stash29", item.getInventoryId());
        assertEquals(0, item.getSocketedItems().size());
    }

    @Test
    void currencySplinter() throws FileNotFoundException, MalformedURLException {
        String json = getTestJson("itemImpl/category_currency_splinter.json");

        ItemImpl item = gson.fromJson(json, ItemImpl.class);

        assertFalse(item.isVerified());
        assertEquals(1, item.getW());
        assertEquals(1, item.getH());
        assertEquals(0, item.getIlvl());
        assertEquals(new URL("https://web.poecdn.com/image/Art/2DItems/Currency/Breach/BreachShardFire.png?scale=1&w=1&h=1&v=4635e0847323cf1d62c8b4e8101351bf"), item.getIcon());
        assertEquals("Synthesis", item.getLeague());
        assertEquals("d2a04aecfaa9da4b8835e4274cebaf8e9db05ba8467172281dfb65f6b12b514e", item.getId());
        assertEquals("", item.getName());
        assertEquals("Splinter of Xoph", item.getTypeLine());
        assertTrue(item.isIdentified());
        assertEquals(1, item.getProperties().size());
        assertEquals("Combine 100 splinters to create Xoph's Breachstone.", item.getDescrText());
        assertEquals(5, item.getFrameType());
        assertEquals(10, (int)item.getStackSize());
        assertEquals(100, (int)item.getMaxStackSize());
        assertEquals(0, item.getCategory().getCurrency().size());
        assertNull(item.getCategory().getArmour());
        assertNull(item.getCategory().getWeapons());
        assertNull(item.getCategory().getJewels());
        assertNull(item.getCategory().getAccessories());
        assertNull(item.getCategory().getCards());
        assertNull(item.getCategory().getGems());
        assertEquals(22, (int)item.getX());
        assertEquals(0, (int)item.getY());
        assertEquals("Stash1", item.getInventoryId());

    }

    @Test
    void flask() throws FileNotFoundException, MalformedURLException {
        String json = getTestJson("itemImpl/category_flask.json");

        ItemImpl item = gson.fromJson(json, ItemImpl.class);

        assertFalse(item.isVerified());
        assertEquals(1, item.getW());
        assertEquals(2, item.getH());
        assertEquals(63, item.getIlvl());
        assertEquals(new URL("https://web.poecdn.com/gen/image/WzksNCx7ImYiOiJBcnRcLzJESXRlbXNcL0ZsYXNrc1wvc3RpYm5pdGUiLCJzcCI6MC42MDg1LCJsZXZlbCI6MH1d/ae37c98a12/Item.png"), item.getIcon());
        assertEquals("Synthesis", item.getLeague());
        assertEquals("a847f696a906b581f9872d12645ce18acd9e15d4bbe88ff55894a1fd3c278af1", item.getId());
        assertEquals("", item.getName());
        assertEquals("Experimenter's Stibnite Flask", item.getTypeLine());
        assertTrue(item.isIdentified());
        assertEquals(3, item.getProperties().size());
        assertEquals(1, item.getRequirements().size());
        assertEquals(1, item.getUtilityMods().size());
        assertEquals(1, item.getExplicitMods().size());
        assertEquals(1, item.getImplicitMods().size());
        assertEquals("~price 1 chrom", item.getNote());
        assertEquals("Right click to drink. Can only hold charges while in belt. Refills as you kill monsters.", item.getDescrText());
        assertEquals(1, item.getFrameType());
        assertNull(item.getStackSize());
        assertNull(item.getMaxStackSize());
        assertEquals(0, item.getCategory().getFlasks().size());
        assertNull(item.getCategory().getArmour());
        assertNull(item.getCategory().getWeapons());
        assertNull(item.getCategory().getJewels());
        assertNull(item.getCategory().getAccessories());
        assertNull(item.getCategory().getCards());
        assertNull(item.getCategory().getGems());
        assertEquals(11, (int)item.getX());
        assertEquals(2, (int)item.getY());
        assertEquals("Stash2", item.getInventoryId());

    }

    @Test
    void gemsActivegem() throws FileNotFoundException, MalformedURLException {
        String json = getTestJson("itemImpl/category_gems_activegem.json");

        ItemImpl item = gson.fromJson(json, ItemImpl.class);

        assertFalse(item.isVerified());
        assertEquals(1, item.getW());
        assertEquals(1, item.getH());
        assertEquals(0, item.getIlvl());
        assertFalse(item.isSupport());
        assertEquals(new URL("https://web.poecdn.com/image/Art/2DItems/Gems/ImmortalCall.png?scale=1&w=1&h=1&v=3843ced383e5dca18e076e57e9f67819"), item.getIcon());
        assertEquals("Standard", item.getLeague());
        assertEquals("174e3e629ec6ead7d8966d6b08c065295a3617a183b5c5ad725002e6a5377db6", item.getId());
        assertEquals("", item.getName());
        assertEquals("Immortal Call", item.getTypeLine());
        assertTrue(item.isIdentified());
        assertEquals(6, item.getProperties().size());
        assertEquals(1, item.getAdditionalProperties().size());
        assertEquals(2, item.getRequirements().size());
        assertNull(item.getNextLevelRequirements());
        assertNull(item.getUtilityMods());
        assertEquals(3, item.getExplicitMods().size());
        assertNull(item.getImplicitMods());
        assertNull(item.getNote());
        assertEquals("Discharges Endurance Charges, making the character invulnerable to physical damage for a short time, proportional to how many endurance charges were expended.", item.secDescrText());
        assertEquals("Place into an item socket of the right colour to gain this skill. Right click to remove from a socket.", item.getDescrText());
        assertEquals(4, item.getFrameType());
        assertNull(item.getStackSize());
        assertNull(item.getMaxStackSize());
        assertEquals(1, item.getCategory().getGems().size());
        assertEquals("activegem", item.getCategory().getGems().get(0));
        assertNull(item.getCategory().getFlasks());
        assertNull(item.getCategory().getArmour());
        assertNull(item.getCategory().getWeapons());
        assertNull(item.getCategory().getJewels());
        assertNull(item.getCategory().getAccessories());
        assertNull(item.getCategory().getCards());
        assertEquals(0, (int)item.getX());
        assertEquals(4, (int)item.getY());
        assertEquals("Stash85", item.getInventoryId());

    }

    @Test
    void gemsSupportgem() throws MalformedURLException, FileNotFoundException {
        String json = getTestJson("itemImpl/category_gems_supportgem.json");

        ItemImpl item = gson.fromJson(json, ItemImpl.class);

        assertFalse(item.isVerified());
        assertEquals(1, item.getW());
        assertEquals(1, item.getH());
        assertEquals(0, item.getIlvl());
        assertTrue(item.isSupport());
        assertEquals(new URL("https://web.poecdn.com/image/Art/2DItems/Gems/Support/TrapAndMineDamage.png?scale=1&w=1&h=1&v=aa1fd5a62b528839c08f0f19148d1541"), item.getIcon());
        assertEquals("Standard", item.getLeague());
        assertEquals("a6d0190b85fef1a2d2ad407697c32a1b690a49a6b2772d28a3433d4f92a0d079", item.getId());
        assertEquals("", item.getName());
        assertEquals("Trap and Mine Damage Support", item.getTypeLine());
        assertTrue(item.isIdentified());
        assertEquals(4, item.getProperties().size());
        assertEquals(1, item.getAdditionalProperties().size());
        assertEquals(3, item.getRequirements().size());
        assertEquals(2, item.getNextLevelRequirements().size());
        assertNull(item.getUtilityMods());
        assertEquals(4, item.getExplicitMods().size());
        assertNull(item.getImplicitMods());
        assertNull(item.getNote());
        assertEquals("Supports trap or mine skills.", item.secDescrText());
        assertEquals("This is a Support Gem. It does not grant a bonus to your character, but to skills in sockets connected to it. Place into an item socket connected to a socket containing the Active Skill Gem you wish to augment. Right click to remove from a socket.", item.getDescrText());
        assertEquals(4, item.getFrameType());
        assertNull(item.getStackSize());
        assertNull(item.getMaxStackSize());
        assertEquals(1, item.getCategory().getGems().size());
        assertEquals("supportgem", item.getCategory().getGems().get(0));
        assertNull(item.getCategory().getFlasks());
        assertNull(item.getCategory().getArmour());
        assertNull(item.getCategory().getWeapons());
        assertNull(item.getCategory().getJewels());
        assertNull(item.getCategory().getAccessories());
        assertNull(item.getCategory().getCards());
        assertEquals(0, (int)item.getX());
        assertEquals(11, (int)item.getY());
        assertEquals("Stash81", item.getInventoryId());

    }

    @Test
    void jewel() throws FileNotFoundException, MalformedURLException {
        String json = getTestJson("itemImpl/category_jewels.json");

        ItemImpl item = gson.fromJson(json, ItemImpl.class);

        assertFalse(item.isVerified());
        assertEquals(1, item.getW());
        assertEquals(1, item.getH());
        assertEquals(81, item.getIlvl());
        assertEquals(new URL("https://web.poecdn.com/image/Art/2DItems/Jewels/basicint.png?scale=1&w=1&h=1&v=cd579ea22c05f1c6ad2fd015d7a710bd"), item.getIcon());
        assertEquals("Synthesis", item.getLeague());
        assertEquals("d5e91b3c3bac540c1f50d4acf49dbda03189a64b52ab8102342f8c49c2bbd2a3", item.getId());
        assertEquals("Hypnotic Stone", item.getName());
        assertEquals("Cobalt Jewel", item.getTypeLine());
        assertTrue(item.isIdentified());
        assertNull(item.getProperties());
        assertNull(item.getAdditionalProperties());
        assertNull(item.getRequirements());
        assertNull(item.getUtilityMods());
        assertEquals(4, item.getExplicitMods().size());
        assertNull(item.getImplicitMods());
        assertEquals("~price 5 chaos", item.getNote());
        assertEquals("Place into an allocated Jewel Socket on the Passive Skill Tree. Right click to remove from the Socket.", item.getDescrText());
        assertEquals(2, item.getFrameType());
        assertNull(item.getStackSize());
        assertNull(item.getMaxStackSize());
        assertEquals(0, item.getCategory().getJewels().size());
        assertNull(item.getCategory().getFlasks());
        assertNull(item.getCategory().getArmour());
        assertNull(item.getCategory().getWeapons());
        assertNull(item.getCategory().getGems());
        assertNull(item.getCategory().getAccessories());
        assertNull(item.getCategory().getCards());
        assertEquals(11, (int)item.getX());
        assertEquals(6, (int)item.getY());
        assertEquals("Stash11", item.getInventoryId());

    }

    @Test
    void jewelAbyss() throws FileNotFoundException, MalformedURLException {
        String json = getTestJson("itemImpl/category_jewels_abyss.json");

        ItemImpl item = gson.fromJson(json, ItemImpl.class);

        assertFalse(item.isVerified());
        assertEquals(1, item.getW());
        assertEquals(1, item.getH());
        assertEquals(82, item.getIlvl());
        assertEquals(new URL("https://web.poecdn.com/image/Art/2DItems/Jewels/RivetedEye.png?scale=1&w=1&h=1&v=a0904494de195adde172f2d6dd329b05"), item.getIcon());
        assertEquals("Synthesis", item.getLeague());
        assertEquals("17d40c72a25cb3ea7e69c1dbcc5a96679fd1789364aded1df67e2bbe1496f742", item.getId());
        assertTrue(item.isAbyssJewel());
        assertEquals("Ancient Leer", item.getName());
        assertEquals("Hypnotic Eye Jewel", item.getTypeLine());
        assertTrue(item.isIdentified());
        assertNull(item.getAdditionalProperties());
        assertNull(item.getUtilityMods());
        assertEquals(1, item.getProperties().size());
        assertEquals(1, item.getRequirements().size());
        assertEquals(3, item.getExplicitMods().size());
        assertNull(item.getImplicitMods());
        assertEquals("~price 35 chaos", item.getNote());
        assertEquals("Place into an Abyssal Socket on an Item or into an allocated Jewel Socket on the Passive Skill Tree. Right click to remove from the Socket.", item.getDescrText());
        assertEquals(2, item.getFrameType());
        assertNull(item.getStackSize());
        assertNull(item.getMaxStackSize());
        assertEquals(1, item.getCategory().getJewels().size());
        assertEquals("abyss", item.getCategory().getJewels().get(0));
        assertNull(item.getCategory().getFlasks());
        assertNull(item.getCategory().getArmour());
        assertNull(item.getCategory().getWeapons());
        assertNull(item.getCategory().getGems());
        assertNull(item.getCategory().getAccessories());
        assertNull(item.getCategory().getCards());
        assertEquals(11, (int)item.getX());
        assertEquals(8, (int)item.getY());
        assertEquals("Stash63", item.getInventoryId());
    }

    @Test
    void maps() throws FileNotFoundException, MalformedURLException {
        String json = getTestJson("itemImpl/category_maps.json");

        ItemImpl item = gson.fromJson(json, ItemImpl.class);

        assertFalse(item.isVerified());
        assertEquals(1, item.getW());
        assertEquals(1, item.getH());
        assertEquals(82, item.getIlvl());
        assertEquals(new URL("https://web.poecdn.com/image/Art/2DItems/Maps/Atlas2Maps/New/IvoryTemple.png?scale=1&w=1&h=1&mn=3&mt=14&v=dc8aa329f320dba5d3e823263c28c258"), item.getIcon());
        assertEquals("Synthesis", item.getLeague());
        assertEquals("c0e431d64fb4e6461328b243ace59e2e306d13d0756f1d01ddbced0997030b88", item.getId());
        assertNull(item.isAbyssJewel());
        assertEquals("", item.getName());
        assertEquals("Ivory Temple Map", item.getTypeLine());
        assertTrue(item.isIdentified());
        assertNull(item.getAdditionalProperties());
        assertNull(item.getUtilityMods());
        assertEquals(1, item.getProperties().size());
        assertNull(item.getRequirements());
        assertNull(item.getExplicitMods());
        assertNull(item.getImplicitMods());
        assertNull(item.getNote());
        assertEquals("Travel to this Map by using it in the Templar Laboratory or a personal Map Device. Maps can only be used once.", item.getDescrText());
        assertEquals(0, item.getFrameType());
        assertNull(item.getStackSize());
        assertNull(item.getMaxStackSize());
        assertEquals(0, item.getCategory().getMaps().size());
        assertNull(item.getCategory().getJewels());
        assertNull(item.getCategory().getFlasks());
        assertNull(item.getCategory().getArmour());
        assertNull(item.getCategory().getWeapons());
        assertNull(item.getCategory().getGems());
        assertNull(item.getCategory().getAccessories());
        assertNull(item.getCategory().getCards());
        assertEquals(0, (int)item.getX());
        assertEquals(1, (int)item.getY());
        assertEquals("Stash1", item.getInventoryId());
    }

    @Test
    void mapsScarab() throws FileNotFoundException, MalformedURLException {
        String json = getTestJson("itemImpl/category_maps_scarab.json");

        ItemImpl item = gson.fromJson(json, ItemImpl.class);

        assertFalse(item.isVerified());
        assertEquals(1, item.getW());
        assertEquals(1, item.getH());
        assertEquals(0, item.getIlvl());
        assertEquals(new URL("https://web.poecdn.com/image/Art/2DItems/Currency/Scarabs/LesserScarabBeasts.png?scale=1&w=1&h=1&v=ddb914ed1c2b799263e2e12f73d8f8fc"), item.getIcon());
        assertEquals("Synthesis", item.getLeague());
        assertEquals("65ee69cb58b94a946802a1abd00d088ba73e657b0218d2693d438fb6af41af64", item.getId());
        assertNull(item.isAbyssJewel());
        assertEquals("", item.getName());
        assertEquals("Rusted Bestiary Scarab", item.getTypeLine());
        assertTrue(item.isIdentified());
        assertNull(item.getAdditionalProperties());
        assertNull(item.getUtilityMods());
        assertNull(item.getProperties());
        assertNull(item.getRequirements());
        assertEquals(1, item.getExplicitMods().size());
        assertNull(item.getImplicitMods());
        assertNull(item.getNote());
        assertEquals("Can be used in the Templar Laboratory or a personal Map Device to add modifiers to a Map.", item.getDescrText());
        assertEquals(2, item.getFlavourText().size());
        assertEquals(0, item.getFrameType());
        assertEquals(1, (int)item.getStackSize());
        assertEquals(5000, (int)item.getMaxStackSize());
        assertEquals(new ArrayList<String>() {{
            add("fragment");
            add("scarab");
        }}, item.getCategory().getMaps());
        assertNull(item.getCategory().getJewels());
        assertNull(item.getCategory().getFlasks());
        assertNull(item.getCategory().getArmour());
        assertNull(item.getCategory().getWeapons());
        assertNull(item.getCategory().getGems());
        assertNull(item.getCategory().getAccessories());
        assertNull(item.getCategory().getCards());
        assertEquals(37, (int)item.getX());
        assertEquals(0, (int)item.getY());
        assertEquals("Stash4", item.getInventoryId());
    }

    @Test
    void monsters() throws FileNotFoundException, MalformedURLException {
        String json = getTestJson("itemImpl/category_monsters.json");

        ItemImpl item = gson.fromJson(json, ItemImpl.class);

        assertFalse(item.isVerified());
        assertEquals(1, item.getW());
        assertEquals(1, item.getH());
        assertEquals(71, item.getIlvl());
        assertEquals(new URL("https://web.poecdn.com/image/Art/2DItems/Currency/BestiaryOrbFull.png?scale=1&w=1&h=1&v=0d0f6c8509eaf231513fc8b9a5df4626"), item.getIcon());
        assertEquals("Standard", item.getLeague());
        assertEquals("c7c8d4612bee57377de022a19e18ff62e35d193b7214650a2e96095910a9e344", item.getId());
        assertNull(item.isAbyssJewel());
        assertEquals("Filthcrusher", item.getName());
        assertEquals("Craicic Vassal", item.getTypeLine());
        assertTrue(item.isIdentified());
        assertNull(item.getAdditionalProperties());
        assertNull(item.getUtilityMods());
        assertEquals(3, item.getProperties().size());
        assertNull(item.getRequirements());
        assertEquals(5, item.getExplicitMods().size());
        assertNull(item.getImplicitMods());
        assertNull(item.getNote());
        assertEquals("Right-click to add this to your bestiary.", item.getDescrText());
        assertNull(item.getFlavourText());
        assertEquals(2, item.getFrameType());
        assertNull(item.getStackSize());
        assertNull(item.getMaxStackSize());
        assertEquals(0, item.getCategory().getMonsters().size());
        assertNull(item.getCategory().getMaps());
        assertNull(item.getCategory().getJewels());
        assertNull(item.getCategory().getFlasks());
        assertNull(item.getCategory().getArmour());
        assertNull(item.getCategory().getWeapons());
        assertNull(item.getCategory().getGems());
        assertNull(item.getCategory().getAccessories());
        assertNull(item.getCategory().getCards());
        assertEquals(0, (int)item.getX());
        assertEquals(3, (int)item.getY());
        assertEquals("Stash6", item.getInventoryId());
    }

    @Test
    void weaponsWand() throws FileNotFoundException, MalformedURLException {
        String json = getTestJson("itemImpl/category_weapons_wand.json");

        ItemImpl item = gson.fromJson(json, ItemImpl.class);

        assertFalse(item.isVerified());
        assertEquals(1, item.getW());
        assertEquals(3, item.getH());
        assertEquals(77, item.getIlvl());
        assertEquals(new URL("https://web.poecdn.com/image/Art/2DItems/Weapons/OneHandWeapons/Wands/Wand6.png?scale=1&w=1&h=3&v=54ba351a078c22737d02dfcec8466585"), item.getIcon());
        assertEquals("Synthesis", item.getLeague());
        assertEquals("76223614050f7752c16e8388f31a3b0b0e59b7bbb6bafde2b44cdfd4abf51475", item.getId());
        assertNull(item.isAbyssJewel());
        assertEquals(3, item.getSockets().size());
        assertEquals("Fate Branch", item.getName());
        assertEquals("Prophecy Wand", item.getTypeLine());
        assertNull(item.isDuplicated());
        assertTrue(item.isIdentified());
        assertNull(item.getAdditionalProperties());
        assertNull(item.getUtilityMods());
        assertEquals(4, item.getProperties().size());
        assertEquals(2, item.getRequirements().size());
        assertEquals(4, item.getExplicitMods().size());
        assertEquals(1, item.getImplicitMods().size());
        assertNull(item.getNote());
        assertNull(item.getDescrText());
        assertNull(item.getFlavourText());
        assertEquals(2, item.getFrameType());
        assertNull(item.getStackSize());
        assertNull(item.getMaxStackSize());
        assertNull( item.getCategory().getMonsters());
        assertNull(item.getCategory().getMaps());
        assertNull(item.getCategory().getJewels());
        assertNull(item.getCategory().getFlasks());
        assertNull(item.getCategory().getArmour());
        assertEquals(1, item.getCategory().getWeapons().size());
        assertEquals("wand", item.getCategory().getWeapons().get(0));
        assertNull(item.getCategory().getGems());
        assertNull(item.getCategory().getAccessories());
        assertNull(item.getCategory().getCards());
        assertEquals(5, (int)item.getX());
        assertEquals(2, (int)item.getY());
        assertEquals("Stash2", item.getInventoryId());
        assertEquals(0, item.getSocketedItems().size());
    }

    @Test
    void itemWithSocketedItems() throws FileNotFoundException, MalformedURLException {
        String json = getTestJson("itemImpl/item_with_socketedItems.json");

        ItemImpl item = gson.fromJson(json, ItemImpl.class);

        assertFalse(item.isVerified());
        assertEquals(2, item.getW());
        assertEquals(4, item.getH());
        assertEquals(78, item.getIlvl());
        assertEquals(new URL("https://web.poecdn.com/image/Art/2DItems/Weapons/TwoHandWeapons/Bows/Bow8.png?scale=1&duplicated=1&w=2&h=4&v=6c31d15f02f212101bb0bcdb0bbe5c86"), item.getIcon());
        assertEquals("Standard", item.getLeague());
        assertEquals("5d90c23f41899de7fafb337275eedf03985e5860237355c5361d94ed3fe6cd75", item.getId());
        assertNull(item.isAbyssJewel());
        assertEquals(6, item.getSockets().size());
        assertEquals("Glyph Mark", item.getName());
        assertEquals("Harbinger Bow", item.getTypeLine());
        assertTrue(item.isDuplicated());
        assertTrue(item.isIdentified());
        assertNull(item.getAdditionalProperties());
        assertNull(item.getUtilityMods());
        assertEquals(5, item.getProperties().size());
        assertEquals(4, item.getRequirements().size());
        assertEquals(6, item.getExplicitMods().size());
        assertEquals(1, item.getImplicitMods().size());
        assertEquals("~price 175 exa", item.getNote());
        assertNull(item.getDescrText());
        assertNull(item.getFlavourText());
        assertEquals(2, item.getFrameType());
        assertNull(item.getStackSize());
        assertNull(item.getMaxStackSize());
        assertNull(item.getCategory().getMonsters());
        assertNull(item.getCategory().getMaps());
        assertNull(item.getCategory().getJewels());
        assertNull(item.getCategory().getFlasks());
        assertNull(item.getCategory().getArmour());
        assertEquals(1, item.getCategory().getWeapons().size());
        assertEquals("bow", item.getCategory().getWeapons().get(0));
        assertNull(item.getCategory().getGems());
        assertNull(item.getCategory().getAccessories());
        assertNull(item.getCategory().getCards());
        assertEquals(10, (int)item.getX());
        assertEquals(0, (int)item.getY());
        assertEquals("Stash9", item.getInventoryId());
        assertEquals(6, item.getSocketedItems().size());
        assertNull(item.getColour());

        // socketed item
        assertFalse(item.getSocketedItems().get(0).isVerified());
        assertEquals(1, item.getSocketedItems().get(0).getW());
        assertEquals(1, item.getSocketedItems().get(0).getH());
        assertEquals(0, item.getSocketedItems().get(0).getIlvl());
        assertEquals(new URL("https://web.poecdn.com/image/Art/2DItems/Gems/Support/IncreasedQuantity.png?scale=1&w=1&h=1&v=b07c717ecf613d726e9edcbc0d66ff93"), item.getSocketedItems().get(0).getIcon());
        assertTrue(item.getSocketedItems().get(0).isSupport());
        assertEquals("Standard", item.getSocketedItems().get(0).getLeague());
        assertEquals("1e3349d178ba4ba23e58eb79e6b27f1f3e81e7f3999a6ef7b9539fd1a195ce0f", item.getSocketedItems().get(0).getId());
        assertNull(item.getSocketedItems().get(0).isAbyssJewel());
        assertEquals("", item.getSocketedItems().get(0).getName());
        assertEquals("Item Quantity Support", item.getSocketedItems().get(0).getTypeLine());
        assertTrue(item.getSocketedItems().get(0).isCorrupted());
        assertTrue(item.getSocketedItems().get(0).isIdentified());
        assertNull(item.getSocketedItems().get(0).getAdditionalProperties());
        assertNull(item.getSocketedItems().get(0).getUtilityMods());
        assertEquals(3, item.getSocketedItems().get(0).getProperties().size());
        assertEquals(2, item.getSocketedItems().get(0).getRequirements().size());
        assertEquals(1, item.getSocketedItems().get(0).getExplicitMods().size());
        assertNull(item.getSocketedItems().get(0).getImplicitMods());
        assertEquals("Supports any skill that can kill enemies.", item.getSocketedItems().get(0).secDescrText());
        assertEquals("This is a Support Gem. It does not grant a bonus to your character, but to skills in sockets connected to it. Place into an item socket connected to a socket containing the Active Skill Gem you wish to augment. Right click to remove from a socket.", item.getSocketedItems().get(0).getDescrText());
        assertNull(item.getSocketedItems().get(0).getFlavourText());
        assertEquals(4, item.getSocketedItems().get(0).getFrameType());
        assertNull(item.getSocketedItems().get(0).getStackSize());
        assertNull(item.getSocketedItems().get(0).getMaxStackSize());
        assertNull(item.getSocketedItems().get(0).getCategory().getMonsters());
        assertNull(item.getSocketedItems().get(0).getCategory().getMaps());
        assertNull(item.getSocketedItems().get(0).getCategory().getJewels());
        assertNull(item.getSocketedItems().get(0).getCategory().getFlasks());
        assertNull(item.getSocketedItems().get(0).getCategory().getArmour());
        assertNull(item.getSocketedItems().get(0).getCategory().getWeapons());
        assertEquals(1, item.getSocketedItems().get(0).getCategory().getGems().size());
        assertEquals("supportgem", item.getSocketedItems().get(0).getCategory().getGems().get(0));
        assertNull(item.getSocketedItems().get(0).getCategory().getAccessories());
        assertNull(item.getSocketedItems().get(0).getCategory().getCards());
        assertNull(item.getSocketedItems().get(0).getX());
        assertNull(item.getSocketedItems().get(0).getY());
        assertNull(item.getSocketedItems().get(0).getInventoryId());
        assertNull(item.getSocketedItems().get(0).getSocketedItems());
        assertEquals("S", item.getSocketedItems().get(0).getColour());
    }

    @Test
    void itemSpecialElder() throws FileNotFoundException, MalformedURLException {
        String json = getTestJson("itemImpl/item_special_elder.json");

        ItemImpl item = gson.fromJson(json, ItemImpl.class);

        assertFalse(item.isVerified());
        assertEquals(2, item.getW());
        assertEquals(2, item.getH());
        assertEquals(81, item.getIlvl());
        assertEquals(new URL("https://web.poecdn.com/image/Art/2DItems/Armours/Boots/BootsDex3.png?scale=1&w=2&h=2&v=597ff9da61bbd5547ef2b10d25b8e909"), item.getIcon());
        assertEquals("Synthesis", item.getLeague());
        assertEquals("83993d606a2c0f5fd0a6815d9cde85dd98aecbebf31bd919e703bd7f79614471", item.getId());
        assertTrue(item.isElder());
        assertNull(item.isFractured());
        assertNull(item.isShaper());
        assertNull(item.isAbyssJewel());
        assertEquals(2, item.getSockets().size());
        assertEquals("Dread Goad", item.getName());
        assertEquals("Sharkskin Boots", item.getTypeLine());
        assertNull(item.isDuplicated());
        assertTrue(item.isIdentified());
        assertNull(item.getAdditionalProperties());
        assertNull(item.getUtilityMods());
        assertEquals(1, item.getProperties().size());
        assertEquals(2, item.getRequirements().size());
        assertEquals(5, item.getExplicitMods().size());
        assertNull(item.getFracturedMods());
        assertNull(item.getImplicitMods());
        assertNull(item.getNote());
        assertNull(item.getDescrText());
        assertNull(item.getFlavourText());
        assertEquals(2, item.getFrameType());
        assertNull(item.getStackSize());
        assertNull(item.getMaxStackSize());
        assertNull( item.getCategory().getMonsters());
        assertNull(item.getCategory().getMaps());
        assertNull(item.getCategory().getJewels());
        assertNull(item.getCategory().getFlasks());
        assertNull(item.getCategory().getWeapons());
        assertEquals(1, item.getCategory().getArmour().size());
        assertEquals("boots", item.getCategory().getArmour().get(0));
        assertNull(item.getCategory().getGems());
        assertNull(item.getCategory().getAccessories());
        assertNull(item.getCategory().getCards());
        assertEquals(6, (int)item.getX());
        assertEquals(4, (int)item.getY());
        assertEquals("Stash1", item.getInventoryId());
        assertEquals(0, item.getSocketedItems().size());
    }

    @Test
    void itemSpecialFractured() throws FileNotFoundException, MalformedURLException {
        String json = getTestJson("itemImpl/item_special_fractured.json");

        ItemImpl item = gson.fromJson(json, ItemImpl.class);

        assertFalse(item.isVerified());
        assertEquals(1, item.getW());
        assertEquals(3, item.getH());
        assertEquals(82, item.getIlvl());
        assertEquals(new URL("https://web.poecdn.com/image/Art/2DItems/Weapons/OneHandWeapons/Wands/Wand3.png?scale=1&w=1&h=3&fractured=1&v=ac9f7eee09f5a8f580151298712f2a9e"), item.getIcon());
        assertEquals("Synthesis", item.getLeague());
        assertEquals("84dcc90fbcb365f4ad719f2d8f5a6fd6d1afbc7e44d0e908b4006de7ba468e5b", item.getId());
        assertNull(item.isElder());
        assertTrue(item.isFractured());
        assertNull(item.isShaper());
        assertNull(item.isAbyssJewel());
        assertEquals(2, item.getSockets().size());
        assertEquals("Gloom Barb", item.getName());
        assertEquals("Imbued Wand", item.getTypeLine());
        assertNull(item.isDuplicated());
        assertTrue(item.isIdentified());
        assertNull(item.getAdditionalProperties());
        assertNull(item.getUtilityMods());
        assertEquals(4, item.getProperties().size());
        assertEquals(2, item.getRequirements().size());
        assertEquals(3, item.getExplicitMods().size());
        assertEquals(1, item.getImplicitMods().size());
        assertEquals(1, item.getFracturedMods().size());
        assertEquals("~price 35 chaos", item.getNote());
        assertNull(item.getDescrText());
        assertNull(item.getFlavourText());
        assertEquals(2, item.getFrameType());
        assertNull(item.getStackSize());
        assertNull(item.getMaxStackSize());
        assertNull( item.getCategory().getMonsters());
        assertNull(item.getCategory().getMaps());
        assertNull(item.getCategory().getJewels());
        assertNull(item.getCategory().getFlasks());
        assertNull(item.getCategory().getArmour());
        assertEquals(1, item.getCategory().getWeapons().size());
        assertEquals("wand", item.getCategory().getWeapons().get(0));
        assertNull(item.getCategory().getGems());
        assertNull(item.getCategory().getAccessories());
        assertNull(item.getCategory().getCards());
        assertEquals(4, (int)item.getX());
        assertEquals(7, (int)item.getY());
        assertEquals("Stash29", item.getInventoryId());
        assertEquals(0, item.getSocketedItems().size());
    }

    @Test
    void itemSpecialShaper() throws FileNotFoundException, MalformedURLException {
        String json = getTestJson("itemImpl/item_special_shaper.json");

        ItemImpl item = gson.fromJson(json, ItemImpl.class);

        assertFalse(item.isVerified());
        assertEquals(1, item.getW());
        assertEquals(3, item.getH());
        assertEquals(81, item.getIlvl());
        assertEquals(new URL("https://web.poecdn.com/image/Art/2DItems/Weapons/OneHandWeapons/Wands/Wand2.png?scale=1&w=1&h=3&v=dd4e9f9ef8b06e3f3e3fa3ba400f8ae8"), item.getIcon());
        assertEquals("Synthesis", item.getLeague());
        assertEquals("b1bad19b25e7a571d459297388ec156535b9f1713c8eaa03ab6ae590cefa2c7b", item.getId());
        assertNull(item.isElder());
        assertNull(item.isFractured());
        assertTrue(item.isShaper());
        assertNull(item.isAbyssJewel());
        assertEquals(3, item.getSockets().size());
        assertEquals("Pain Goad", item.getName());
        assertEquals("Demon's Horn", item.getTypeLine());
        assertNull(item.isDuplicated());
        assertTrue(item.isIdentified());
        assertNull(item.getAdditionalProperties());
        assertNull(item.getUtilityMods());
        assertEquals(4, item.getProperties().size());
        assertEquals(2, item.getRequirements().size());
        assertEquals(6, item.getExplicitMods().size());
        assertEquals(1, item.getImplicitMods().size());
        assertNull(item.getCraftedMods());
        assertNull(item.getFracturedMods());
        assertEquals("~price 8 chaos", item.getNote());
        assertNull(item.getDescrText());
        assertNull(item.getFlavourText());
        assertEquals(2, item.getFrameType());
        assertNull(item.getStackSize());
        assertNull(item.getMaxStackSize());
        assertNull(item.getCategory().getMonsters());
        assertNull(item.getCategory().getMaps());
        assertNull(item.getCategory().getJewels());
        assertNull(item.getCategory().getFlasks());
        assertNull(item.getCategory().getArmour());
        assertEquals(1, item.getCategory().getWeapons().size());
        assertEquals("wand", item.getCategory().getWeapons().get(0));
        assertNull(item.getCategory().getGems());
        assertNull(item.getCategory().getAccessories());
        assertNull(item.getCategory().getCards());
        assertEquals(21, (int)item.getX());
        assertEquals(6, (int)item.getY());
        assertEquals("Stash62", item.getInventoryId());
        assertEquals(0, item.getSocketedItems().size());
    }

    @Test
    void itemSpecialCrafted() throws FileNotFoundException, MalformedURLException {
        String json = getTestJson("itemImpl/item_special_crafted.json");

        ItemImpl item = gson.fromJson(json, ItemImpl.class);

        assertFalse(item.isVerified());
        assertEquals(1, item.getW());
        assertEquals(1, item.getH());
        assertEquals(73, item.getIlvl());
        assertEquals(new URL("https://web.poecdn.com/image/Art/2DItems/Amulets/Amulet6.png?scale=1&w=1&h=1&v=bc7ee6ba515d5f34cfaf7daec1571c52"), item.getIcon());
        assertEquals("Standard", item.getLeague());
        assertEquals("566536c7fb1fa78645587770f4eb82be899a4aa653ca9a59d2b45e7a388234b3", item.getId());
        assertNull(item.isElder());
        assertNull(item.isFractured());
        assertNull(item.isShaper());
        assertNull(item.isAbyssJewel());
        assertNull(item.getSockets());
        assertEquals("Brood Medallion", item.getName());
        assertEquals("Gold Amulet", item.getTypeLine());
        assertNull(item.isDuplicated());
        assertTrue(item.isIdentified());
        assertNull(item.getAdditionalProperties());
        assertNull(item.getUtilityMods());
        assertNull(item.getProperties());
        assertEquals(1, item.getRequirements().size());
        assertEquals(5, item.getExplicitMods().size());
        assertEquals(1, item.getImplicitMods().size());
        assertEquals(1, item.getCraftedMods().size());
        assertNull(item.getFracturedMods());
        assertEquals("~price 35 chaos", item.getNote());
        assertNull(item.getDescrText());
        assertNull(item.getFlavourText());
        assertEquals(2, item.getFrameType());
        assertNull(item.getStackSize());
        assertNull(item.getMaxStackSize());
        assertNull(item.getCategory().getMonsters());
        assertNull(item.getCategory().getMaps());
        assertNull(item.getCategory().getJewels());
        assertNull(item.getCategory().getFlasks());
        assertNull(item.getCategory().getArmour());
        assertEquals(1, item.getCategory().getAccessories().size());
        assertEquals("amulet", item.getCategory().getAccessories().get(0));
        assertNull(item.getCategory().getGems());
        assertNull(item.getCategory().getWeapons());
        assertNull(item.getCategory().getCards());
        assertEquals(0, (int)item.getX());
        assertEquals(8, (int)item.getY());
        assertEquals("Stash46", item.getInventoryId());
        assertNull(item.getSocketedItems());
    }
}