/*
   Copyright 2019 Anthony Stramer <anthony@stramer.org>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */

package org.stramer.lib.poe;

import com.google.gson.GsonBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

import static org.junit.jupiter.api.Assertions.*;

class LeagueImplTest extends UnitTest {
    private DateFormat dateFormat;

    @BeforeEach
    void setup() {
        gson = new GsonBuilder()
                .registerTypeAdapter(LeagueImpl.class, new LeagueImpl(configuration))
                .registerTypeAdapter(LeagueRule.class, new LeagueRuleImpl(configuration))
                .registerTypeAdapter(LeagueRulesList.class, new LeagueRulesListImpl())
                .create();

        dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    @Test
    void league() throws MalformedURLException, ParseException, FileNotFoundException {
        String json = getTestJson("leagueImpl/league.json");

        LeagueImpl league = gson.fromJson(json, LeagueImpl.class);
        assertEquals("Hardcore", league.getId());
        assertEquals("pc", league.getRealm());
        assertEquals("#LeagueHardcore", league.getDescription());
        assertEquals(new URL("http://pathofexile.com/forum/view-thread/71276"), league.getUrl());
        assertEquals(dateFormat.parse("2013-01-23T21:00:00Z"), league.getStartAt());
        assertNull(league.getEndAt());
        assertTrue(league.getDelveEvent());
        assertEquals(1, league.getRules().size());

        assertEquals("LeagueImpl{id='Hardcore', realm='pc', description='#LeagueHardcore', url=http://pathofexile.com/forum/view-thread/71276, startAt=Wed Jan 23 21:00:00 UTC 2013, endAt=null, delveEvent=true, rules=[LeagueRuleImpl{id='Hardcore', name='Hardcore', description='A character killed in Hardcore is moved to its parent league.'}]}", league.toString());
    }

}