/*
   Copyright 2019 Anthony Stramer <anthony@stramer.org>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */

package org.stramer.lib.poe;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

class LaddersTest extends IntegrationTest {
    private DateFormat dateFormat;

    @BeforeEach
    void setup() {
        dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    @Test
    void betrayalLadderTest() throws FileNotFoundException, PathOfExileException, ParseException {
        LaddersList ladders = mockAPIResponse("ladders/betrayal.json", LaddersList.class);

        // Overall Ladder
        Assertions.assertEquals(dateFormat.parse("2019-03-04T21:00:07Z"), ladders.getCachedSince());
        Assertions.assertEquals(5, ladders.getTotal());

        // Spot checks
        Assertions.assertEquals(1, ladders.get(0).getRank());
        Assertions.assertEquals(37, ladders.get(0).getAccount().getChallenges().getTotal());

        Assertions.assertEquals(5, ladders.get(4).getRank());
        Assertions.assertEquals(29, ladders.get(4).getAccount().getChallenges().getTotal());

    }

    @Test
    void synthesisLadderTest() throws FileNotFoundException, PathOfExileException, ParseException {
        LaddersList ladders = mockAPIResponse("ladders/synthesis.json", LaddersList.class);

        // Overall Ladder
        Assertions.assertEquals(dateFormat.parse("2019-03-31T06:16:06Z"), ladders.getCachedSince());
        Assertions.assertEquals(15000, ladders.getTotal());

        // Spot checks
        Assertions.assertEquals(2, ladders.get(1).getRank());
        Assertions.assertEquals(35, ladders.get(1).getAccount().getChallenges().getTotal());

        Assertions.assertEquals(5, ladders.get(4).getRank());
        Assertions.assertEquals(15, ladders.get(4).getAccount().getChallenges().getTotal());
    }

    @Test
    void hardcoreLadderTest() throws FileNotFoundException, PathOfExileException, ParseException {
        LaddersList ladders = mockAPIResponse("ladders/hardcore.json", LaddersList.class);

        // Overall Ladder
        Assertions.assertEquals(dateFormat.parse("2018-03-31T06:25:04Z"), ladders.getCachedSince());
        Assertions.assertEquals(10, ladders.getTotal());

        // Spot checks
        Assertions.assertEquals(1, ladders.get(0).getRank());
        Assertions.assertEquals(5, ladders.get(0).getAccount().getChallenges().getTotal());
        Assertions.assertFalse(ladders.get(0).isDead());

        Assertions.assertEquals(10, ladders.get(9).getRank());
        Assertions.assertEquals(0, ladders.get(9).getAccount().getChallenges().getTotal());
        Assertions.assertTrue(ladders.get(9).isDead());
    }

    @Test
    void standardLadderTest() throws FileNotFoundException, PathOfExileException, ParseException {
        LaddersList ladders = mockAPIResponse("ladders/standard.json", LaddersList.class);

        // Overall Ladder
        Assertions.assertEquals(dateFormat.parse("2019-03-31T06:24:03Z"), ladders.getCachedSince());
        Assertions.assertEquals(15000, ladders.getTotal());

        // Spot checks
        Assertions.assertEquals(1, ladders.get(0).getRank());
        Assertions.assertEquals(0, ladders.get(0).getAccount().getChallenges().getTotal());

        Assertions.assertEquals(5, ladders.get(4).getRank());
        Assertions.assertEquals(19, ladders.get(4).getAccount().getChallenges().getTotal());
    }
}
