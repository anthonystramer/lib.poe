/*
   Copyright 2019 Anthony Stramer <anthony@stramer.org>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */

package org.stramer.lib.poe;

import com.google.gson.GsonBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.*;

public class StrictProcessingTest extends UnitTest {

    @BeforeEach
    void setup() { gson = new GsonBuilder()
            .registerTypeAdapter(ItemImpl.class, new ItemImpl(configuration))
            .registerTypeAdapter(Item.class, new ItemImpl(configuration))
            .registerTypeAdapter(Category.class, new CategoryImpl(configuration))
            .registerTypeAdapter(ModsList.class, new ModsListImpl())
            .registerTypeAdapter(FlavourTextList.class, new FlavourTextListImpl())
            .registerTypeAdapter(Property.class, new PropertyImpl(configuration))
            .registerTypeAdapter(PropertiesList.class, new PropertiesListImpl())
            .registerTypeAdapter(Socket.class, new SocketImpl(configuration))
            .registerTypeAdapter(SocketsList.class, new SocketsListImpl())
            .registerTypeAdapter(SocketedItemsList.class, new SocketedItemsListImpl())
            .registerTypeAdapter(Value.class, new ValueImpl(configuration))
            .registerTypeAdapter(ValuesList.class, new ValuesListImpl())
            .create();
    }

    @Test
    void noErrors() throws FileNotFoundException {
        PrintStream systemOut = System.out;
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        final PrintStream stream = new PrintStream(out);
        System.setOut(stream);

        String json = getTestJson("strictProcessing/strict_no_errors.json");

        ItemImpl item = gson.fromJson(json, ItemImpl.class);

        assertFalse(item.isVerified());
        assertEquals(1, item.getW());
        assertEquals(1, item.getH());
        assertEquals(75, item.getIlvl());

        assertFalse(out.toString().contains("Unprocessed key in request"));

        System.setOut(systemOut);
        System.out.print(out.toString());
    }

    @Test
    void oneError() throws FileNotFoundException {
        PrintStream systemOut = System.out;
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        final PrintStream stream = new PrintStream(out);
        System.setOut(stream);

        String json = getTestJson("strictProcessing/strict_one_error.json");

        ItemImpl item = gson.fromJson(json, ItemImpl.class);

        assertFalse(item.isVerified());
        assertEquals(1, item.getW());
        assertEquals(1, item.getH());
        assertEquals(75, item.getIlvl());

        assertTrue(out.toString().contains("Unprocessed key in request, key: errorOne"));

        System.setOut(systemOut);
        System.out.print(out.toString());
    }

    @Test
    void twoErrors() throws FileNotFoundException {
        PrintStream systemOut = System.out;
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        final PrintStream stream = new PrintStream(out);
        System.setOut(stream);

        String json = getTestJson("strictProcessing/strict_two_errors.json");

        ItemImpl item = gson.fromJson(json, ItemImpl.class);

        assertFalse(item.isVerified());
        assertEquals(1, item.getW());
        assertEquals(1, item.getH());
        assertEquals(75, item.getIlvl());

        assertTrue(out.toString().contains("Unprocessed key in request, key: errorOne"));
        assertTrue(out.toString().contains("Unprocessed key in request, key: errorTwo"));

        System.setOut(systemOut);
        System.out.print(out.toString());

    }
}
