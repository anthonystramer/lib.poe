/*
   Copyright 2019 Anthony Stramer <anthony@stramer.org>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */

package org.stramer.lib.poe;

import com.google.gson.GsonBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;

import static org.junit.jupiter.api.Assertions.*;

class PublicStashTabImplTest extends UnitTest {

    @BeforeEach
    void setup() {
        gson = new GsonBuilder()
                .registerTypeAdapter(PublicStashTabImpl.class, new PublicStashTabImpl(configuration))
                .registerTypeAdapter(Item.class, new ItemImpl(configuration))
                .registerTypeAdapter(ItemsList.class, new ItemsListImpl())
                .registerTypeAdapter(Category.class, new CategoryImpl(configuration))
                .registerTypeAdapter(ModsList.class, new ModsListImpl())
                .registerTypeAdapter(FlavourTextList.class, new FlavourTextListImpl())
                .registerTypeAdapter(Property.class, new PropertyImpl(configuration))
                .registerTypeAdapter(PropertiesList.class, new PropertiesListImpl())
                .registerTypeAdapter(Socket.class, new SocketImpl(configuration))
                .registerTypeAdapter(SocketsList.class, new SocketsListImpl())
                .registerTypeAdapter(SocketedItemsList.class, new SocketedItemsListImpl())
                .registerTypeAdapter(Value.class, new ValueImpl(configuration))
                .registerTypeAdapter(ValuesList.class, new ValuesListImpl())
                .create();
    }

    @Test
    void stashPrivateNormal() throws FileNotFoundException {
        String json = getTestJson("publicStashTabImpl/stash_private_normal.json");

        PublicStashTabImpl publicStashTab  = gson.fromJson(json, PublicStashTabImpl.class);
        assertEquals("3fd287034b13c6880513134d64fe13109dfde80fe57e4d58a9c7991b7d632c9c", publicStashTab.getId());
        assertFalse(publicStashTab.isPublic());
        assertNull(publicStashTab.getAccountName());
        assertNull(publicStashTab.getLastCharacterName());
        assertNull(publicStashTab.getStash());
        assertEquals("NormalStash", publicStashTab.getStashType());
        assertEquals("Standard", publicStashTab.getLeague());
        assertEquals(0, publicStashTab.getItemsList().size());
    }

    @Test
    void stashPublicMap() throws FileNotFoundException {
        String json = getTestJson("publicStashTabImpl/stash_public_map.json");

        PublicStashTabImpl publicStashTab = gson.fromJson(json, PublicStashTabImpl.class);
        assertEquals("ed0c40ee338b3d680d08ca83bda9f3376d7c0f8664c9df781bd64f14b39f2731", publicStashTab.getId());
        assertTrue(publicStashTab.isPublic());
        assertEquals("IgrindEightDaysAweek", publicStashTab.getAccountName());
        assertEquals("top_ten_or_bust", publicStashTab.getLastCharacterName());
        assertEquals("map", publicStashTab.getStash());
        assertEquals("MapStash", publicStashTab.getStashType());
        assertEquals("Synthesis", publicStashTab.getLeague());
        assertEquals(11, publicStashTab.getItemsList().size());
    }

}