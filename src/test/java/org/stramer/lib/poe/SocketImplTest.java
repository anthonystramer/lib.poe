/*
   Copyright 2019 Anthony Stramer <anthony@stramer.org>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */

package org.stramer.lib.poe;

import com.google.gson.GsonBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SocketImplTest extends UnitTest {
    @BeforeEach
    void setup() {
        gson = new GsonBuilder()
                .registerTypeAdapter(SocketImpl.class, new SocketImpl(configuration))
                .create();
    }

    @Test
    void standardSocket() {
        String json = "{\"group\": 0, \"attr\": \"S\", \"sColour\": \"R\"}";

        SocketImpl socket = gson.fromJson(json, SocketImpl.class);
        assertEquals(0, socket.getGroup());
        assertEquals("S", socket.getAttr());
        assertEquals("R", socket.getsColour());
    }

    @Test
    void whiteSocket() {
        String json = "{\"group\": 0, \"attr\": \"G\", \"sColour\": \"W\"}";

        SocketImpl socket = gson.fromJson(json, SocketImpl.class);
        assertEquals(0, socket.getGroup());
        assertEquals("G", socket.getAttr());
        assertEquals("W", socket.getsColour());
    }

    @Test
    void abyssalSocket() {
        String json = "{\"group\": 1, \"attr\": \"A\", \"sColour\": \"A\"}";

        SocketImpl socket = gson.fromJson(json, SocketImpl.class);
        assertEquals(1, socket.getGroup());
        assertEquals("A", socket.getAttr());
        assertEquals("A", socket.getsColour());
    }

    @Test
    void delveSocket() {
        String json = "{\"group\": 0, \"attr\": \"DV\", \"sColour\": \"DV\"}";

        SocketImpl socket = gson.fromJson(json, SocketImpl.class);
        assertEquals(0, socket.getGroup());
        assertEquals("DV", socket.getAttr());
        assertEquals("DV", socket.getsColour());
    }


}