/*
   Copyright 2019 Anthony Stramer <anthony@stramer.org>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */

package org.stramer.lib.poe;

import com.google.gson.GsonBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class AccountImplTest extends UnitTest {

    @BeforeEach
    void setup() {
        gson = new GsonBuilder()
                .registerTypeAdapter(AccountImpl.class, new AccountImpl(configuration))
                .registerTypeAdapter(Challenges.class, new ChallengesImpl(configuration))
                .registerTypeAdapter(Twitch.class, new TwitchImpl(configuration))
                .create();
    }

    @Test
    void deserializeWithOnlyRequiredFields() {
        String json = "{\"name\": \"Storbusen\",\"realm\": \"pc\",\"challenges\": {\"total\": 0}}";

        AccountImpl account = gson.fromJson(json, AccountImpl.class);
        assertEquals("Storbusen", account.getName());
        assertEquals("pc", account.getRealm());
        assertEquals(0, account.getChallenges().getTotal());
        assertNull(account.getTwitch());

        assertEquals("AccountImpl{name='Storbusen', realm='pc', challenges=ChallengesImpl{total=0}, twitch=null}", account.toString());

    }

    @Test
    void deserializeWithOptionalFields() {
        String json = "{\"name\": \"itsMartini\",\"realm\": \"sony\",\"challenges\": {\"total\": 29},\"twitch\": {\"name\": \"itsmartini\"}}";

        AccountImpl account = gson.fromJson(json, AccountImpl.class);
        assertEquals("itsMartini", account.getName());
        assertEquals("sony", account.getRealm());
        assertEquals(29, account.getChallenges().getTotal());
        assertEquals("itsmartini", account.getTwitch().getName());

        assertEquals("AccountImpl{name='itsMartini', realm='sony', challenges=ChallengesImpl{total=29}, twitch=TwitchImpl{name='itsmartini'}}", account.toString());
    }

}