/*
   Copyright 2019 Anthony Stramer <anthony@stramer.org>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */

package org.stramer.lib.poe;

import com.google.gson.GsonBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;

import static org.junit.jupiter.api.Assertions.*;

class PvPMatchesListImplTest extends UnitTest {
    @BeforeEach
    void setup() {
        gson = new GsonBuilder()
                .registerTypeAdapter(PvPMatchesListImpl.class, new PvPMatchesListImpl())
                .registerTypeAdapter(PvPMatch.class, new PvPMatchImpl(configuration))
                .create();
    }

    @Test
    void PvPMatchesList() throws FileNotFoundException {
        String json = getTestJson("pvpMatchesListImpl/eupvpseason1.json");

        PvPMatchesListImpl pvpMatchesList = gson.fromJson(json, PvPMatchesListImpl.class);

        assertEquals(92, pvpMatchesList.size());
    }
}