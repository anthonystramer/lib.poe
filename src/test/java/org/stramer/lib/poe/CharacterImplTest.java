/*
   Copyright 2019 Anthony Stramer <anthony@stramer.org>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */

package org.stramer.lib.poe;

import com.google.gson.GsonBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class CharacterImplTest extends UnitTest {

    @BeforeEach
    void setup() {
        gson = new GsonBuilder()
                .registerTypeAdapter(CharacterImpl.class, new CharacterImpl(configuration))
                .registerTypeAdapter(Depth.class, new DepthImpl(configuration))
                .create();
    }

    @Test
    void characterWithOnlyRequiredFields() {
        String json = "{\n" +
                "        \"name\": \"SasusiaTesusia\",\n" +
                "        \"level\": 100,\n" +
                "        \"class\": \"Pathfinder\",\n" +
                "        \"id\": \"c93359f2d95bbdbf14ad0adf3f54868fb08a98bba6a40dd0c7bf7c17216cecdb\",\n" +
                "        \"experience\": 4250334444\n" +
                "      }";

        CharacterImpl character = gson.fromJson(json, CharacterImpl.class);
        assertEquals("SasusiaTesusia", character.getName());
        assertEquals(100, character.getLevel());
        assertEquals("Pathfinder", character.getCharacterClass());
        assertEquals("c93359f2d95bbdbf14ad0adf3f54868fb08a98bba6a40dd0c7bf7c17216cecdb", character.getId());
        assertEquals(4250334444L, character.getExperience());
        assertNull(character.getDepth());

        assertEquals("CharacterImpl{name='SasusiaTesusia', level=100, characterClass='Pathfinder', id='c93359f2d95bbdbf14ad0adf3f54868fb08a98bba6a40dd0c7bf7c17216cecdb', experience=4250334444, depth=null}", character.toString());
    }

    @Test
    void characterWithOptionalFields() {
        String json = "{\n" +
                "        \"name\": \"Tzn_KaruiLivesMatter\",\n" +
                "        \"level\": 99,\n" +
                "        \"class\": \"Occultist\",\n" +
                "        \"id\": \"94691c0e4202a5c01a111294a91561647f1961130dd419788cb256c566b87986\",\n" +
                "        \"experience\": 4250334444,\n" +
                "        \"depth\": {\n" +
                "          \"default\": 308,\n" +
                "          \"solo\": 308\n" +
                "        }\n" +
                "      }";

        CharacterImpl character = gson.fromJson(json, CharacterImpl.class);
        assertEquals("Tzn_KaruiLivesMatter", character.getName());
        assertEquals(99, character.getLevel());
        assertEquals("Occultist", character.getCharacterClass());
        assertEquals("94691c0e4202a5c01a111294a91561647f1961130dd419788cb256c566b87986", character.getId());
        assertEquals(4250334444L, character.getExperience());
        assertEquals(308, character.getDepth().getDefault());
        assertEquals(308, character.getDepth().getSolo());

        assertEquals("CharacterImpl{name='Tzn_KaruiLivesMatter', level=99, characterClass='Occultist', id='94691c0e4202a5c01a111294a91561647f1961130dd419788cb256c566b87986', experience=4250334444, depth=DepthImpl{default=308, solo=308}}", character.toString());
    }
}