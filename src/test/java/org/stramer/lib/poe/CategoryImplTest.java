/*
   Copyright 2019 Anthony Stramer <anthony@stramer.org>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */

package org.stramer.lib.poe;

import com.google.gson.GsonBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class CategoryImplTest extends UnitTest {

    @BeforeEach
    void setup() {
        gson = new GsonBuilder()
                .registerTypeAdapter(CategoryImpl.class, new CategoryImpl(configuration))
                .create();
    }

    @Test
    void accessoriesAmulet() {
        String json = "{\"accessories\": [\"amulet\"]}";

        CategoryImpl category = gson.fromJson(json, CategoryImpl.class);
        assertEquals(Collections.singletonList("amulet"), category.getAccessories());
        assertNull(category.getArmour());
        assertNull(category.getJewels());
        assertNull(category.getWeapons());
        assertNull(category.getCards());
        assertNull(category.getCurrency());
        assertNull(category.getFlasks());
        assertNull(category.getGems());
        assertNull(category.getMaps());
        assertNull(category.getMonsters());

        assertEquals("CategoryImpl{accessories=[amulet], armour=null, jewels=null, weapons=null, cards=null, currency=null, flasks=null, gems=null, maps=null, monsters=null}", category.toString());
    }

    @Test
    void accessoriesRing() {
        String json = "{\"accessories\": [\"ring\"]}";

        CategoryImpl category = gson.fromJson(json, CategoryImpl.class);
        assertEquals(Collections.singletonList("ring"), category.getAccessories());
        assertNull(category.getArmour());
        assertNull(category.getJewels());
        assertNull(category.getWeapons());
        assertNull(category.getCards());
        assertNull(category.getCurrency());
        assertNull(category.getFlasks());
        assertNull(category.getGems());
        assertNull(category.getMaps());
        assertNull(category.getMonsters());

        assertEquals("CategoryImpl{accessories=[ring], armour=null, jewels=null, weapons=null, cards=null, currency=null, flasks=null, gems=null, maps=null, monsters=null}", category.toString());
    }

    @Test
    void armourHelmet() {
        String json = "{\"armour\": [\"helmet\"]}";

        CategoryImpl category = gson.fromJson(json, CategoryImpl.class);
        assertEquals(Collections.singletonList("helmet"), category.getArmour());
        assertNull(category.getAccessories());
        assertNull(category.getJewels());
        assertNull(category.getWeapons());
        assertNull(category.getCards());
        assertNull(category.getCurrency());
        assertNull(category.getFlasks());
        assertNull(category.getGems());
        assertNull(category.getMaps());
        assertNull(category.getMonsters());

        assertEquals("CategoryImpl{accessories=null, armour=[helmet], jewels=null, weapons=null, cards=null, currency=null, flasks=null, gems=null, maps=null, monsters=null}", category.toString());
    }

    @Test
    void armourQuiver() {
        String json = "{\"armour\": [\"quiver\"]}";

        CategoryImpl category = gson.fromJson(json, CategoryImpl.class);
        assertEquals(Collections.singletonList("quiver"), category.getArmour());
        assertNull(category.getAccessories());
        assertNull(category.getJewels());
        assertNull(category.getWeapons());
        assertNull(category.getCards());
        assertNull(category.getCurrency());
        assertNull(category.getFlasks());
        assertNull(category.getGems());
        assertNull(category.getMaps());
        assertNull(category.getMonsters());

        assertEquals("CategoryImpl{accessories=null, armour=[quiver], jewels=null, weapons=null, cards=null, currency=null, flasks=null, gems=null, maps=null, monsters=null}", category.toString());
    }

    @Test
    void jewels() {
        String json = "{\"jewels\": []}";

        CategoryImpl category = gson.fromJson(json, CategoryImpl.class);
        assertEquals(new ArrayList<>(), category.getJewels());
        assertNull(category.getArmour());
        assertNull(category.getAccessories());
        assertNull(category.getWeapons());
        assertNull(category.getCards());
        assertNull(category.getCurrency());
        assertNull(category.getFlasks());
        assertNull(category.getGems());
        assertNull(category.getMaps());
        assertNull(category.getMonsters());

        assertEquals("CategoryImpl{accessories=null, armour=null, jewels=[], weapons=null, cards=null, currency=null, flasks=null, gems=null, maps=null, monsters=null}", category.toString());

    }

    @Test
    void jewelsAbyss() {
        String json = "{\"jewels\": [\"abyss\"]}";

        CategoryImpl category = gson.fromJson(json, CategoryImpl.class);
        assertEquals(Collections.singletonList("abyss"), category.getJewels());
        assertNull(category.getArmour());
        assertNull(category.getAccessories());
        assertNull(category.getWeapons());
        assertNull(category.getCards());
        assertNull(category.getCurrency());
        assertNull(category.getFlasks());
        assertNull(category.getGems());
        assertNull(category.getMaps());
        assertNull(category.getMonsters());

        assertEquals("CategoryImpl{accessories=null, armour=null, jewels=[abyss], weapons=null, cards=null, currency=null, flasks=null, gems=null, maps=null, monsters=null}", category.toString());

    }

    @Test
    void weaponsBow() {
        String json = "{\"weapons\": [\"bow\"]}";

        CategoryImpl category = gson.fromJson(json, CategoryImpl.class);
        assertEquals(Collections.singletonList("bow"), category.getWeapons());
        assertNull(category.getArmour());
        assertNull(category.getAccessories());
        assertNull(category.getJewels());
        assertNull(category.getCards());
        assertNull(category.getCurrency());
        assertNull(category.getFlasks());
        assertNull(category.getGems());
        assertNull(category.getMaps());
        assertNull(category.getMonsters());

        assertEquals("CategoryImpl{accessories=null, armour=null, jewels=null, weapons=[bow], cards=null, currency=null, flasks=null, gems=null, maps=null, monsters=null}", category.toString());
    }

    @Test
    void weaponsWand() {
        String json = "{\"weapons\": [\"wand\"]}";

        CategoryImpl category = gson.fromJson(json, CategoryImpl.class);
        assertEquals(Collections.singletonList("wand"), category.getWeapons());
        assertNull(category.getArmour());
        assertNull(category.getAccessories());
        assertNull(category.getJewels());
        assertNull(category.getCards());
        assertNull(category.getCurrency());
        assertNull(category.getFlasks());
        assertNull(category.getGems());
        assertNull(category.getMaps());
        assertNull(category.getMonsters());

        assertEquals("CategoryImpl{accessories=null, armour=null, jewels=null, weapons=[wand], cards=null, currency=null, flasks=null, gems=null, maps=null, monsters=null}", category.toString());
    }

    @Test
    void cards() {
        String json = "{\"cards\": []}";

        CategoryImpl category = gson.fromJson(json, CategoryImpl.class);
        assertEquals(new ArrayList<String>(), category.getCards());
        assertNull(category.getWeapons());
        assertNull(category.getArmour());
        assertNull(category.getAccessories());
        assertNull(category.getJewels());
        assertNull(category.getCurrency());
        assertNull(category.getFlasks());
        assertNull(category.getGems());
        assertNull(category.getMaps());
        assertNull(category.getMonsters());

        assertEquals("CategoryImpl{accessories=null, armour=null, jewels=null, weapons=null, cards=[], currency=null, flasks=null, gems=null, maps=null, monsters=null}", category.toString());
    }

    @Test
    void currency() {
        String json = "{\"currency\": [\"resonator\"]}";

        CategoryImpl category = gson.fromJson(json, CategoryImpl.class);
        assertEquals(Collections.singletonList("resonator"), category.getCurrency());
        assertNull(category.getArmour());
        assertNull(category.getAccessories());
        assertNull(category.getJewels());
        assertNull(category.getCards());
        assertNull(category.getWeapons());
        assertNull(category.getFlasks());
        assertNull(category.getGems());
        assertNull(category.getMaps());
        assertNull(category.getMonsters());

        assertEquals("CategoryImpl{accessories=null, armour=null, jewels=null, weapons=null, cards=null, currency=[resonator], flasks=null, gems=null, maps=null, monsters=null}", category.toString());
    }

    @Test
    void flasks() {
        String json = "{\"flasks\": []}";

        CategoryImpl category = gson.fromJson(json, CategoryImpl.class);
        assertEquals(0, category.getFlasks().size());
        assertNull(category.getArmour());
        assertNull(category.getAccessories());
        assertNull(category.getJewels());
        assertNull(category.getCards());
        assertNull(category.getWeapons());
        assertNull(category.getCurrency());
        assertNull(category.getGems());
        assertNull(category.getMaps());
        assertNull(category.getMonsters());

        assertEquals("CategoryImpl{accessories=null, armour=null, jewels=null, weapons=null, cards=null, currency=null, flasks=[], gems=null, maps=null, monsters=null}", category.toString());
    }

    @Test
    void gemsActive() {
        String json = "{\"gems\": [\"activegem\"]}";

        CategoryImpl category = gson.fromJson(json, CategoryImpl.class);
        assertEquals(1, category.getGems().size());
        assertEquals("activegem", category.getGems().get(0));
        assertNull(category.getArmour());
        assertNull(category.getAccessories());
        assertNull(category.getJewels());
        assertNull(category.getCards());
        assertNull(category.getWeapons());
        assertNull(category.getCurrency());
        assertNull(category.getFlasks());
        assertNull(category.getMaps());
        assertNull(category.getMonsters());

        assertEquals("CategoryImpl{accessories=null, armour=null, jewels=null, weapons=null, cards=null, currency=null, flasks=null, gems=[activegem], maps=null, monsters=null}", category.toString());
    }

    @Test
    void maps() {
        String json = "{\"maps\": []}";

        CategoryImpl category = gson.fromJson(json, CategoryImpl.class);
        assertEquals(0, category.getMaps().size());
        assertNull(category.getArmour());
        assertNull(category.getAccessories());
        assertNull(category.getJewels());
        assertNull(category.getCards());
        assertNull(category.getWeapons());
        assertNull(category.getCurrency());
        assertNull(category.getFlasks());
        assertNull(category.getGems());
        assertNull(category.getMonsters());

        assertEquals("CategoryImpl{accessories=null, armour=null, jewels=null, weapons=null, cards=null, currency=null, flasks=null, gems=null, maps=[], monsters=null}", category.toString());
    }

    @Test
    void monsters() {
        String json = "{\"monsters\": []}";

        CategoryImpl category = gson.fromJson(json, CategoryImpl.class);
        assertEquals(0, category.getMonsters().size());
        assertNull(category.getArmour());
        assertNull(category.getAccessories());
        assertNull(category.getJewels());
        assertNull(category.getCards());
        assertNull(category.getWeapons());
        assertNull(category.getCurrency());
        assertNull(category.getFlasks());
        assertNull(category.getGems());
        assertNull(category.getMaps());

        assertEquals("CategoryImpl{accessories=null, armour=null, jewels=null, weapons=null, cards=null, currency=null, flasks=null, gems=null, maps=null, monsters=[]}", category.toString());
    }
}