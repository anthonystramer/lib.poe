/*
   Copyright 2019 Anthony Stramer <anthony@stramer.org>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */

package org.stramer.lib.poe;

import com.google.gson.GsonBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

import static org.junit.jupiter.api.Assertions.*;

class PvPMatchImplTest extends UnitTest {
    private DateFormat dateFormat;

    @BeforeEach
    void setup() {
        gson = new GsonBuilder()
                .registerTypeAdapter(PvPMatch.class, new PvPMatchImpl(configuration))
                .create();

        dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    @Test
    void PvPMatch() throws FileNotFoundException, ParseException, MalformedURLException {
        String json = getTestJson("pvpMatchImpl/pvpmatch.json");

        PvPMatchImpl pvpMatch = gson.fromJson(json, PvPMatchImpl.class);

        assertEquals("EU01-71-STD FFA Arena", pvpMatch.getId());
        assertEquals("pc", pvpMatch.getRealm());
        assertEquals(dateFormat.parse("2015-01-10T15:30:00Z"), pvpMatch.getStartAt());
        assertEquals(dateFormat.parse("2015-01-10T15:50:00Z"), pvpMatch.getEndAt());
        assertEquals(new URL("http://pathofexile.com/forum/view-thread/1166479"), pvpMatch.getUrl());
        assertEquals("Low Level Free For All", pvpMatch.getDescription());
        assertFalse(pvpMatch.isGlickoRatings());
        assertTrue(pvpMatch.isPvp());
        assertEquals("Arena", pvpMatch.getStyle());
        assertEquals(dateFormat.parse("2015-01-10T15:20:00Z"), pvpMatch.getRegisterAt());

    }
}