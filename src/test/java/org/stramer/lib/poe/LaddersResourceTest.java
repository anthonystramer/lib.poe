/*
   Copyright 2019 Anthony Stramer <anthony@stramer.org>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */

package org.stramer.lib.poe;

import org.junit.jupiter.api.Test;
import org.stramer.lib.poe.api.LaddersResourcesOptions;
import org.stramer.lib.poe.api.Realm;

import static org.junit.jupiter.api.Assertions.*;

class LaddersResourceTest extends IntegrationTest {
    @Test
    void ladders() throws PathOfExileException {
        PathOfExile pathOfExile = PathOfExileFactory.getSingleton();

        LaddersList laddersList = pathOfExile.ladders().getLadders("Standard");

        System.out.println(laddersList.getTotal());
        System.out.println(laddersList.getCachedSince());

        for(Ladder ladder : laddersList) {
            System.out.println(ladder);
        }

        assertEquals("pc", laddersList.get(0).getAccount().getRealm());
    }

    @Test
    void laddersWithOptions() throws PathOfExileException {
        PathOfExile pathOfExile = PathOfExileFactory.getSingleton();

        LaddersResourcesOptions options = new LaddersResourcesOptions();
        options.setRealm(Realm.SONY);
        LaddersList laddersList = pathOfExile.ladders().getLadders("Standard", options);

        System.out.println(laddersList.getTotal());
        System.out.println(laddersList.getCachedSince());

        for(Ladder ladder : laddersList) {
            System.out.println(ladder);
        }

        assertEquals("sony", laddersList.get(0).getAccount().getRealm());
    }
}
