/*
   Copyright 2019 Anthony Stramer <anthony@stramer.org>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */

package org.stramer.lib.poe;

import com.google.gson.GsonBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PropertyImplTest extends UnitTest {

    @BeforeEach
    void setup() {
        gson = new GsonBuilder()
                .registerTypeAdapter(PropertyImpl.class, new PropertyImpl(configuration))
                .registerTypeAdapter(ValuesList.class, new ValuesListImpl())
                .registerTypeAdapter(Value.class, new ValueImpl(configuration))
                .create();
    }

    @Test
    void property() {
        String json = " {\n" +
                "           \"name\": \"Stack Size\",\n" +
                "           \"values\": [\n" +
                "               [\n" +
                "                   \"1/9\",\n" +
                "                   0\n" +
                "               ]\n" +
                "           ],\n" +
                "           \"displayMode\": 0\n" +
                "       }";

        PropertyImpl property = gson.fromJson(json, PropertyImpl.class);

        assertEquals("Stack Size", property.getName());
        assertEquals(1, property.getValues().size());
        assertEquals(0, property.getDisplayMode());

        assertEquals("PropertyImpl{name='Stack Size', values=[ValueImpl{value='1/9', valueType=0}], displayMode=0, type=null, progress=null}", property.toString());
    }

    @Test
    void additionalProperty() {
        String json = " {\n" +
                "           \"name\": \"Experience\",\n" +
                "           \"values\": [\n" +
                "               [\n" +
                "                   \"1/212046017\",\n" +
                "                   0\n" +
                "               ]\n" +
                "           ],\n" +
                "           \"displayMode\": 2,\n" +
                "           \"progress\": 4.715957402368076e-9,\n" +
                "           \"type\": 20\n" +
                "       }";

        PropertyImpl property = gson.fromJson(json, PropertyImpl.class);

        assertEquals("Experience", property.getName());
        assertEquals(1, property.getValues().size());
        assertEquals(2, property.getDisplayMode());
        assertEquals(Integer.valueOf(20), property.getType());
        assertEquals(Double.valueOf(4.715957402368076E-9), property.getProgress());

        assertEquals("PropertyImpl{name='Experience', values=[ValueImpl{value='1/212046017', valueType=0}], displayMode=2, type=20, progress=4.715957402368076E-9}", property.toString());

    }

}