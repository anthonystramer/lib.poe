/*
   Copyright 2019 Anthony Stramer <anthony@stramer.org>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */

package org.stramer.lib.poe;

import com.google.gson.Gson;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.stramer.lib.poe.conf.*;

import java.io.*;
import java.util.TimeZone;

@Tag("unit")
public abstract class UnitTest {
    Gson gson;
    static Configuration configuration = new ConfigurationBuilder().setProcessingStrictness(ProcessingStrictness.STRICT).setDebug(true).setLoggerFactory("org.stramer.lib.poe.StdOutLoggerFactory").build();

    @BeforeAll
    static void setTimezone() {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
    }

    @BeforeAll
    static void setConfigurationContext() {
        ConfigurationContext.setConfiguration(configuration);
    }

    String getTestJson(String filename) throws FileNotFoundException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource(filename).getFile());
        InputStreamReader reader = new InputStreamReader(new FileInputStream(file));

        StringBuilder contentBuilder = new StringBuilder();
        try (BufferedReader br = new BufferedReader(reader))
        {

            String sCurrentLine;
            while ((sCurrentLine = br.readLine()) != null)
            {
                contentBuilder.append(sCurrentLine).append("\n");
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return contentBuilder.toString();
    }
}
