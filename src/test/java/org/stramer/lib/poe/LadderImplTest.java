/*
   Copyright 2019 Anthony Stramer <anthony@stramer.org>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */

package org.stramer.lib.poe;

import com.google.gson.GsonBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LadderImplTest extends UnitTest {

    @BeforeEach
    void setup() {
        gson = new GsonBuilder()
                .registerTypeAdapter(LadderImpl.class, new LadderImpl(configuration))
                .registerTypeAdapter(Character.class, new CharacterImpl(configuration))
                .registerTypeAdapter(Account.class, new AccountImpl(configuration))
                .registerTypeAdapter(Challenges.class, new ChallengesImpl(configuration))
                .registerTypeAdapter(Twitch.class, new TwitchImpl(configuration))
                .create();
    }

    @Test
    void ladderEntry() {
        String json = " {\n" +
                "      \"rank\": 9,\n" +
                "      \"dead\": false,\n" +
                "      \"online\": false,\n" +
                "      \"character\": {\n" +
                "        \"name\": \"SirBaker\",\n" +
                "        \"level\": 100,\n" +
                "        \"class\": \"Shadow\",\n" +
                "        \"id\": \"6442f2b5a488a4ccba6eb5c5039c92c80205ec84b5f1525e0ade893d805f9be5\",\n" +
                "        \"experience\": 4250334444\n" +
                "      },\n" +
                "      \"account\": {\n" +
                "        \"name\": \"Baker\",\n" +
                "        \"realm\": \"pc\",\n" +
                "        \"challenges\": {\n" +
                "          \"total\": 12\n" +
                "        },\n" +
                "        \"twitch\": {\n" +
                "          \"name\": \"bakerlive\"\n" +
                "        }\n" +
                "      }\n" +
                "    }";

        LadderImpl ladder = gson.fromJson(json, LadderImpl.class);

        assertEquals(9, ladder.getRank());
        assertFalse(ladder.isDead());
        assertFalse(ladder.isOnline());
        assertNotNull(ladder.getCharacter());
        assertNotNull(ladder.getAccount());

        assertEquals("LadderImpl{rank=9, dead=false, online=false, character=CharacterImpl{name='SirBaker', level=100, characterClass='Shadow', id='6442f2b5a488a4ccba6eb5c5039c92c80205ec84b5f1525e0ade893d805f9be5', experience=4250334444, depth=null}, account=AccountImpl{name='Baker', realm='pc', challenges=ChallengesImpl{total=12}, twitch=TwitchImpl{name='bakerlive'}}}", ladder.toString());
    }
}