/*
   Copyright 2019 Anthony Stramer <anthony@stramer.org>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */

package org.stramer.lib.poe;

import com.google.gson.GsonBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class LeagueRuleImplTest extends UnitTest {

    @BeforeEach
    void setup() {
        gson = new GsonBuilder()
                .registerTypeAdapter(LeagueRuleImpl.class, new LeagueRuleImpl(configuration))
                .create();
    }

    @Test
    void leagueRule() {
        String json = "{\n" +
                "        \"id\": \"NoParties\",\n" +
                "        \"name\": \"Solo\",\n" +
                "        \"description\": \"You may not party in this league.\"\n" +
                "      }";

        LeagueRuleImpl leagueRule = gson.fromJson(json, LeagueRuleImpl.class);

        assertEquals("NoParties", leagueRule.getId());
        assertEquals("Solo", leagueRule.getName());
        assertEquals("You may not party in this league.", leagueRule.getDescription());

        assertEquals("LeagueRuleImpl{id='NoParties', name='Solo', description='You may not party in this league.'}", leagueRule.toString());
    }

}