/*
   Copyright 2019 Anthony Stramer <anthony@stramer.org>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */

package org.stramer.lib.poe.api;

import org.junit.jupiter.api.Test;
import org.stramer.lib.poe.UnitTest;

import java.util.Map;
import java.util.TreeMap;

import static org.junit.jupiter.api.Assertions.*;

class LaddersResourcesOptionsTest extends UnitTest {
    @Test
    void testLeague() {
        Map<String, String> map = new TreeMap<>();
        map.put("realm", "xbox");
        map.put("limit", "200");
        map.put("offset", "100");
        map.put("type", "league");
        map.put("track", "false");
        map.put("accountName", "Tablewarez");

        LaddersResourcesOptions options = new LaddersResourcesOptions();
        options.setRealm(Realm.XBOX);
        options.setLimit(200);
        options.setOffset(100);
        options.setType(LaddersResourcesOptions.Type.LEAGUE);
        options.setTrack(false);
        options.setAccountName("Tablewarez");
        options.setDifficulty(LaddersResourcesOptions.Difficulty.CRUEL);
        options.setStart(1554940800L);

        assertEquals(map, options.toMap());
    }

    @Test
    void testLabyrinth() {
        Map<String, String> map = new TreeMap<>();
        map.put("realm", "pc");
        map.put("limit", "20");
        map.put("offset", "0");
        map.put("type", "labyrinth");
        map.put("track", "true");
        map.put("difficulty", "2");
        map.put("start", "1554940800");

        LaddersResourcesOptions options = new LaddersResourcesOptions();
        options.setRealm(Realm.PC);
        options.setLimit(20);
        options.setOffset(0);
        options.setType(LaddersResourcesOptions.Type.LABYRINTH);
        options.setTrack(true);
        options.setAccountName("Tablewarez");
        options.setDifficulty(LaddersResourcesOptions.Difficulty.CRUEL);
        options.setStart(1554940800L);

        assertEquals(map, options.toMap());
    }

    @Test
    void testErrors() {
        LaddersResourcesOptions options = new LaddersResourcesOptions();

        assertThrows(IllegalArgumentException.class, () -> options.setLimit(0));
        assertThrows(IllegalArgumentException.class, () -> options.setLimit(-1));
        assertThrows(IllegalArgumentException.class, () -> options.setLimit(201));

        assertThrows(IllegalArgumentException.class, () -> options.setOffset(-1));

        assertThrows(IllegalArgumentException.class, () -> options.setStart(-1L));

        assertThrows(IllegalArgumentException.class, () -> options.setRealm(null));
        assertThrows(IllegalArgumentException.class, () -> options.setLimit(null));
        assertThrows(IllegalArgumentException.class, () -> options.setOffset(null));
        assertThrows(IllegalArgumentException.class, () -> options.setType(null));
    }

}