/*
   Copyright 2019 Anthony Stramer <anthony@stramer.org>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */

package org.stramer.lib.poe.api;

import org.junit.jupiter.api.Test;
import org.stramer.lib.poe.UnitTest;

import java.util.Map;
import java.util.TreeMap;

import static org.junit.jupiter.api.Assertions.*;

class GetLeaguesResourcesOptionsTest extends UnitTest {
    @Test
    void testLadder() {
        Map<String, String> map = new TreeMap<>();
        map.put("realm", "pc");
        map.put("ladder", "1");
        map.put("ladderOffset", "1");
        map.put("ladderLimit", "200");
        map.put("ladderTrack", "1");

        GetLeaguesResourcesOptions options = new GetLeaguesResourcesOptions();

        options.setRealm(Realm.PC);
        options.setLadder(true);
        options.setLadderOffset(1);
        options.setLadderLimit(200);
        options.setLadderTrack(true);

        assertEquals(map, options.toMap());
    }

    @Test
    void testNonLadder() {
        Map<String, String> map = new TreeMap<>();
        map.put("ladder", "0");
        map.put("realm", "sony");

        GetLeaguesResourcesOptions options = new GetLeaguesResourcesOptions();

        options.setRealm(Realm.SONY);
        options.setLadder(false);

        assertEquals(map, options.toMap());
    }

    @Test
    void testExceptions() {
        GetLeaguesResourcesOptions options = new GetLeaguesResourcesOptions();

        assertThrows(IllegalArgumentException.class, () -> options.setRealm(null));

        assertThrows(IllegalArgumentException.class, () -> options.setLadderLimit(-1));
        assertThrows(IllegalArgumentException.class, () -> options.setLadderLimit(0));
        assertThrows(IllegalArgumentException.class, () -> options.setLadderLimit(201));

        assertThrows(IllegalArgumentException.class, () -> options.setLadderOffset(-1));
    }
}