/*
   Copyright 2019 Anthony Stramer <anthony@stramer.org>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */

package org.stramer.lib.poe.api;

import org.junit.jupiter.api.Test;
import org.stramer.lib.poe.UnitTest;

import java.util.Map;
import java.util.TreeMap;

import static org.junit.jupiter.api.Assertions.*;

class ListLeaguesResourcesOptionsTest extends UnitTest {
    @Test
    void testSeasonNonNullLimit() {
        Map<String, String> map = new TreeMap<>();
        map.put("type", "season");
        map.put("realm", "pc");
        map.put("season", "Medallion");
        map.put("compact", "1");
        map.put("limit", "200");
        map.put("offset", "0");

        ListLeaguesResourcesOptions options  = new ListLeaguesResourcesOptions();
        options.setType(ListLeaguesResourcesOptions.Type.SEASON);
        options.setRealm(Realm.PC);
        options.setSeason("Medallion");
        options.setCompact(true);
        options.setLimit(200);
        options.setOffset(0);

        assertEquals(map, options.toMap());
    }


    @Test
    void testSeasonNullLimit() {
        // Note: this request would fail if sent to the API, but we are not checking
        // non-null, non-zero length string requirements on season when type=season
        Map<String, String> map = new TreeMap<>();
        map.put("type", "season");
        map.put("realm", "sony");
        map.put("compact", "0");
        map.put("offset", "1");
        map.put("season", null);

        ListLeaguesResourcesOptions options  = new ListLeaguesResourcesOptions();
        options.setType(ListLeaguesResourcesOptions.Type.SEASON);
        options.setRealm(Realm.SONY);
        options.setCompact(false);
        options.setLimit(null);
        options.setOffset(1);

        assertEquals(map, options.toMap());

        map.put("season", "");
        options.setSeason("");

        assertEquals(map, options.toMap());
    }

    @Test
    void testNonSeason() {
        Map<String, String> map = new TreeMap<>();
        map.put("type", "event");
        map.put("realm", "pc");
        map.put("compact", "1");
        map.put("limit", "200");
        map.put("offset", "0");

        ListLeaguesResourcesOptions options = new ListLeaguesResourcesOptions();
        options.setType(ListLeaguesResourcesOptions.Type.EVENT);
        options.setRealm(Realm.PC);
        options.setSeason("Medallion");
        options.setCompact(true);
        options.setLimit(200);
        options.setOffset(0);

        assertEquals(map, options.toMap());
    }

    @Test
    void testExceptions() {
        ListLeaguesResourcesOptions options = new ListLeaguesResourcesOptions();

        assertThrows(IllegalArgumentException.class, () -> options.setType(null));
        assertThrows(IllegalArgumentException.class, () -> options.setRealm(null));

        assertThrows(IllegalArgumentException.class, () -> options.setLimit(-1));
        assertDoesNotThrow(() -> options.setLimit(null));

        assertThrows(IllegalArgumentException.class, () -> options.setOffset(-1));
    }
}