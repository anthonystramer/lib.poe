/*
   Copyright 2019 Anthony Stramer <anthony@stramer.org>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */

package org.stramer.lib.poe.api;

import org.junit.jupiter.api.Test;
import org.stramer.lib.poe.UnitTest;

import java.util.Map;
import java.util.TreeMap;

import static org.junit.jupiter.api.Assertions.*;

class PvPMatchesResourcesOptionsTest extends UnitTest {
    @Test
    void testWithSeason() {
        Map<String, String> map = new TreeMap<>();
        map.put("realm", "xbox");
        map.put("seasonId", "EUPvPSeason1");

        PvPMatchesResourcesOptions options = new PvPMatchesResourcesOptions();
        options.setRealm(Realm.XBOX);
        options.setSeasonId("EUPvPSeason1");

        assertEquals(map, options.toMap());
    }

    @Test
    void testWithoutSeason() {
        Map<String, String> map = new TreeMap<>();
        map.put("realm", "pc");

        PvPMatchesResourcesOptions options = new PvPMatchesResourcesOptions();

        assertEquals(map, options.toMap());
    }

    @Test
    void testExceptions() {
        PvPMatchesResourcesOptions options = new PvPMatchesResourcesOptions();

        assertThrows(IllegalArgumentException.class, () -> options.setRealm(null));
    }
}