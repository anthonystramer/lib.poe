/*
   Copyright 2019 Anthony Stramer <anthony@stramer.org>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */

package org.stramer.lib.poe;

import com.google.gson.GsonBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

import static org.junit.jupiter.api.Assertions.assertEquals;

class LaddersListImplTest extends UnitTest {
    private DateFormat dateFormat;

    @BeforeEach
    void setup() {
        gson = new GsonBuilder()
                .registerTypeAdapter(LaddersListImpl.class, new LaddersListImpl())
                .registerTypeAdapter(Ladder.class, new LadderImpl(configuration))
                .registerTypeAdapter(Character.class, new CharacterImpl(configuration))
                .registerTypeAdapter(Account.class, new AccountImpl(configuration))
                .registerTypeAdapter(Challenges.class, new ChallengesImpl(configuration))
                .create();

        dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    @Test
    void ladders() throws ParseException, FileNotFoundException {
        String json = getTestJson("laddersListImpl/ladders.json");

        LaddersListImpl ladders = gson.fromJson(json, LaddersListImpl.class);

        assertEquals(1, ladders.getTotal());
        assertEquals(dateFormat.parse("2019-04-01T01:02:04Z"), ladders.getCachedSince());
        assertEquals(1, ladders.size());
        assertEquals(1, ladders.get(0).getRank());

        assertEquals("LaddersListImpl{total=1, cachedSince=Mon Apr 01 01:02:04 UTC 2019, entries=[LadderImpl{rank=1, dead=false, online=false, character=CharacterImpl{name='PenDora', level=100, characterClass='Scion', id='cc248e0d23c849d71b40379d82dfc19b200bdb7b8ac63322f06de6483aaca5ea', experience=4250334444, depth=null}, account=AccountImpl{name='Jin_melike', realm='pc', challenges=ChallengesImpl{total=0}, twitch=null}}]}", ladders.toString());
    }
}