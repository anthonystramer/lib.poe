/*
   Copyright 2019 Anthony Stramer <anthony@stramer.org>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */

package org.stramer.lib.poe.conf;

import org.junit.jupiter.api.Test;
import org.stramer.lib.poe.UnitTest;

import java.net.MalformedURLException;
import java.net.URL;

import static org.junit.jupiter.api.Assertions.*;

class ConfigurationBuilderTest extends UnitTest {
    @Test
    void testDefaults() {
        ConfigurationBuilder builder = new ConfigurationBuilder();

        Configuration base = new ConfigurationBase();
        Configuration custom = builder.build();

        assertEquals(base, custom);
    }

    @Test
    void testCustomConfiguration() throws MalformedURLException {
        String url = "http://www.pathofexile.com/api/ladders";
        ProcessingStrictness strictness = ProcessingStrictness.STRICT;

        ConfigurationBuilder builder = new ConfigurationBuilder()
                                            .setBaseUrl(new URL(url))
                                            .setProcessingStrictness(strictness);

        Configuration configuration = builder.build();

        assertEquals(url, configuration.getBaseURL());
        assertEquals(strictness, configuration.getProcessingStrictness());
    }
}