/*
   Copyright 2019 Anthony Stramer <anthony@stramer.org>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */

package org.stramer.lib.poe;

import org.junit.jupiter.api.Test;

class PathOfExileTest extends IntegrationTest {

    @Test
    void ladders() throws PathOfExileException {
        PathOfExile pathOfExile = PathOfExileFactory.getSingleton();

        LaddersList laddersList = pathOfExile.ladders().getLadders("Standard");

        System.out.println(laddersList.getTotal());
        System.out.println(laddersList.getCachedSince());

        for(Ladder ladder : laddersList) {
            System.out.println(ladder);
        }
    }

    @Test
    void leagueRules() throws PathOfExileException {
        PathOfExile pathOfExile = PathOfExileFactory.getSingleton();

        for(LeagueRule rule : pathOfExile.leagueRules().getLeagueRules()) {
            System.out.println(rule);
        }
    }

    @Test
    void leagues() throws PathOfExileException {
        PathOfExile pathOfExile = PathOfExileFactory.getSingleton();

        for(League league : pathOfExile.leagues().getLeagues()) {
            System.out.println(league);
        }
    }

    @Test
    void publicStashTabs() throws PathOfExileException {
        PathOfExile pathOfExile = PathOfExileFactory.getSingleton();

        PublicStashTabList publicStashTabList = pathOfExile.publicStashTabs().getPublicStashTabs();

        System.out.println(publicStashTabList.getNextChangeId());

        for(PublicStashTab publicStashTab : publicStashTabList) {
            System.out.println(publicStashTab);
        }
    }

    @Test
    void pvpMatches() throws PathOfExileException {
        PathOfExile pathOfExile = PathOfExileFactory.getSingleton();

        for(PvPMatch match : pathOfExile.pvpMatches().getPvPMatches()) {
            System.out.println(match);
        }
    }
}