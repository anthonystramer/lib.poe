# lib.poe - Java Path of Exile API Library
lib.poe is a Java library for communicating with the public Path of Exile API.

### Goals
* Full coverage of the public Path of Exile API
* More usable item API


### Download 
You can download the current SNAPSHOT build of lib.poe from the repository's [downloads](https://bitbucket.org/anthonystramer/lib.poe/downloads/) tab or you can build from source using maven. The library is not yet available on maven central.

[![Bitbucket Pipelines](https://img.shields.io/bitbucket/pipelines/anthonystramer/lib.poe.svg)](https://bitbucket.org/anthonystramer/lib.poe/addon/pipelines/home#!/results/branch/master/page/1)

### Documentation
* [lib.poe User Guide](UserGuide.md): Guide for how to use lib.poe in your own application.
* [Chuanhsing's PoE API (Github)](https://github.com/Chuanhsing/poe-api): OpenAPI documentation for the Path of Exile API.
* [Path of Exile API Docs](https://www.pathofexile.com/developer/docs/api): Developer documentation for Path of Exile API Resources.
* [Path of Exile Website APIs](https://www.pathofexile.com/forum/view-forum/674): Documentation for API changes for Leagues after Bestiary.


### License
lib.poe is released under the [Apache 2.0 license](LICENSE).
```
Copyright 2019 Anthony Stramer <anthony@stramer.org>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```