# lib.poe User Guide

## Installation and Dependencies
[![Bitbucket Pipelines](https://img.shields.io/bitbucket/pipelines/anthonystramer/lib.poe.svg)](https://bitbucket.org/anthonystramer/lib.poe/addon/pipelines/home#!/results/branch/master/page/1)

Snapshot versions of the master branch are available via the repository's [downloads](https://bitbucket.org/anthonystramer/lib.poe/downloads/)  tab. You may also build from source using maven.

### Gradle
```
dependencies {
    implementation 'com.google.code.gson:gson:2.8.5'
}
```

### Maven
```xml
<dependency>
    <groupId>com.google.code.gson</groupId>
    <artifactId>gson</artifactId>
    <version>2.8.5</version>
    <scope>compile</scope>
</dependency>
```

### From Source
Project is built using maven.

#### Test
| Type        | Maven                           |
| ----------- | ------------------------------- |
| All         | `mvn test`                      |
| Unit        | `mvn -Dgroups=unit test`        |
| Integration | `mvn -Dgroups=integration test` | 

#### Local Install
This will install a shaded and unshaded jar.
```
mvn install
```


## Examples
### Default Configuration
```java
PathOfExile pathOfExile = PathOfExileFactory.getSingleton();
```

### Custom Configuration
```java
Configuration config = new ConfigurationBuilder()
                            .setProcessingStrictness(ProcessingStrictness.STRICT)
                            .setDebug(true)
                            .setLoggerFactory("org.stramer.lib.poe.StdOutLoggerFactory")
                            .build();
PathOfExileFactory factory = new PathOfExileFactory(config);
PathOfExile pathOfExile = factory.getInstance();
```

### Ladders
```java
LaddersList laddersList = pathOfExile.ladders().getLadders("Standard");
```

### League Rules
```java
LeagueRulesList leagueRulesList = pathOfExile.leagueRules().getLeagueRules();
```

### PvP Rules
```java
PvPMatchesList pvpMatchesList = pathOfExile.pvpMatches().getPvPMatches();
```

### Public Stash Tabs
```java
PublicStashTabList publicStashTabList = pathOfExile.publicStashTabs().getPublicStashTabs();
```

### Public Stash Tabs with Non-Zero Id
```java
PublicStashTabsResourcesOptions options = new PublicStashTabsResourcesOptions();
options.setId("2947-5165-4180-5175-1708");

PublicStashTabList publicStashTabList = pathOfExile.publicStashTabs().getPublicStashTabs(options);
```